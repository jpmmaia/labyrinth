package builder;

import logic.GameLogic;
import random.IRandom;

/**
 * IMazeBuilder.java - Interface for Maze Builders
 * 
 * @author Jo�o Maia
 */
public interface IMazeBuilder 
{
	/**
	 * Builds a labyrinth
	 * 
	 * @param gameMode			the dragon's mode, see {@link logic.GameLogic.mode}
	 * @param random			an object which implements IRandom interface
	 * @param numberOfDragons	the number of dragons to be generated
	 * @param size				the size of the labyrinth
	 * @return					the GameLogic object which runs the game
	 */
	public GameLogic build(GameLogic.mode gameMode, IRandom random, int numberOfDragons, int size);
}
