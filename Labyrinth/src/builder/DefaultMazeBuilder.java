package builder;

import logic.Coord;
import logic.Dragon;
import logic.DragonMode1;
import logic.DragonMode2;
import logic.DragonMode3;
import logic.Eagle;
import logic.GameLogic;
import logic.Hero;
import logic.Labyrinth;
import logic.Sword;
import random.IRandom;

/**
 * DefaultMazeBuilder.java - Builds a default labyrinth
 * 
 * @author Jo�o
 */
public class DefaultMazeBuilder implements IMazeBuilder
{
	/**
	 * Builds the default labyrinth. The size parameter is not used
	 * 
	 * @see IMazeBuilder#build(logic.GameLogic.mode, IRandom, int, int)
	 */
	public GameLogic build(GameLogic.mode gameMode, IRandom random, int numberOfDragons, int size)
	{
		char map [] [] =
			{
				{ 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X' },
				{ 'X', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'X' },
				{ 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X' },
				{ 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X' },
				{ 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X' },
				{ 'X', ' ', ' ', ' ', ' ', ' ', ' ', 'X', ' ', 'S' },
				{ 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X' },
				{ 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X' },
				{ 'X', ' ', 'X', 'X', ' ', ' ', ' ', ' ', ' ', 'X' },
				{ 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X' },
			};

		Coord exit = new Coord(9, 5);

		Labyrinth labyrinth = new Labyrinth(map, exit);

		Sword sword = new Sword(new Coord(1, 8), labyrinth);
		labyrinth.updateAvatar(sword);

		Coord heroPosition = new Coord(1, 1);
		Hero hero = new Hero(heroPosition, labyrinth);
		labyrinth.updateAvatar(hero);
		
		Eagle eagle = new Eagle(hero, sword, labyrinth);

		

		Dragon [] dragons = new Dragon[] { createDragon(new Coord(1, 3), gameMode, random, labyrinth) };
		labyrinth.updateAvatar(dragons[0]);

		return new GameLogic(labyrinth, hero, eagle, sword, dragons);
	}

	private Dragon createDragon(Coord position, GameLogic.mode gameMode, IRandom random, Labyrinth labyrinth)
	{
		Dragon dragon;

		switch(gameMode)
		{
		case freeze:
			dragon = new DragonMode1(position, labyrinth);
			break;
		case random:
			dragon = new DragonMode2(position, random, labyrinth);
			break;
		default:
			dragon = new DragonMode3(position, random, labyrinth);
			break;
		}

		return dragon;
	}
}
