package builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import logic.Coord;
import logic.Dragon;
import logic.DragonMode1;
import logic.DragonMode2;
import logic.DragonMode3;
import logic.Eagle;
import logic.GameLogic;
import logic.Hero;
import logic.Labyrinth;
import logic.Sword;
import random.IRandom;

/**
 * MazeBuilder.java - Creates a random maze given the size, game mode and number of dragons.
 * 
 * @author Jo�o Maia
 */
public class MazeBuilder implements IMazeBuilder
{
	private char[][] map;
	private int size; 
	private Coord exit;

	/**
	 * Builds a random maze
	 * 
	 * @see IMazeBuilder#build(logic.GameLogic.mode, IRandom, int, int)
	 */
	public GameLogic build(GameLogic.mode gameMode, IRandom random, int numberOfDragons,
			int size) 
	{	
		System.out.println("Maze : " + String.valueOf(size));
		
		this.size = size;
		map = new char[size][size];
		for(char[] row : map)
			Arrays.fill(row, 'X');

		Coord firstCell = calculateFirstCell();
		map[firstCell.y][firstCell.x] = ' ';
		calculateExit(firstCell);
		carvePassagesFrom(firstCell.x, firstCell.y);

		return generateGameLogic(numberOfDragons, gameMode, random);
	}

	private void carvePassagesFrom(int cx, int cy)
	{
		ArrayList<Dir> directions = new ArrayList<Dir>(Arrays.asList(Dir.values()));
		Collections.shuffle(directions);

		for(Dir direction : directions)
		{
			int nx = cx + direction.dx * 2;
			int ny = cy + direction.dy * 2;

			if(nx > 0 && nx < size - 1 && ny > 0 && ny < size - 1 && map[ny][nx] == 'X')
			{
				// Removing the wall
				map[cy + direction.dy][cx + direction.dx] = ' ';

				// Removing the spot
				map[ny][nx] = ' ';

				carvePassagesFrom(nx, ny);
			}
		}
	}

	private GameLogic generateGameLogic(int nDragons, GameLogic.mode gameMode, IRandom random)
	{
		Random rand = new Random();
		Set<Coord> positions = new HashSet<Coord>();

		int nAvatars = 2 + nDragons;
		do
		{
			Coord p = new Coord();
			p.x = rand.nextInt(size - 2) + 1;
			if(p.x % 2 == 0)
				p.x -= 1;

			p.y = rand.nextInt(size - 2) + 1;
			if(p.y % 2 == 0)
				p.y -= 1;

			positions.add(p);
		} while(positions.size() < nAvatars);

		ArrayList<Coord> list = new ArrayList<Coord>(positions);
		Labyrinth labyrinth = new Labyrinth(map, exit);

		Hero hero = new Hero(list.get(0), labyrinth);
		labyrinth.updateAvatar(hero);

		Sword sword = new Sword(list.get(1), labyrinth);
		labyrinth.updateAvatar(sword);

		Eagle eagle= new Eagle(hero, sword, labyrinth);

		Dragon[] dragons = new Dragon[nDragons];
		switch(gameMode)
		{
		case freeze:
			for(int i = 0; i < nDragons; i++)
				dragons[i] = new DragonMode1(list.get(2 + i), labyrinth);
			break;
		case random:
			for(int i = 0; i < nDragons; i++)
				dragons[i] = new DragonMode2(list.get(2 + i), random, labyrinth);
			break;
		case randomSleep:
			for(int i = 0; i < nDragons; i++)
				dragons[i] = new DragonMode3(list.get(2 + i), random, labyrinth);
			break;
		}

		for(Dragon dragon : dragons)
			labyrinth.updateAvatar(dragon);

		return new GameLogic(labyrinth, hero, eagle, sword, dragons);
	}

	private void calculateExit(Coord firstCell)
	{
		exit = new Coord(0, 0);
		if(firstCell.x == 1)
			exit = new Coord(0, firstCell.y);
		else if(firstCell.x == size - 2)
			exit = new Coord(size - 1, firstCell.y);
		else if(firstCell.y == 1)
			exit = new Coord(firstCell.x, 0);
		else if(firstCell.y == size - 2)
			exit = new Coord(firstCell.x, size - 1);

		map[exit.y][exit.x] = 'S';			
	}

	private Coord calculateFirstCell()
	{
		Coord firstCell = new Coord(1, 1);
		Random rand = new Random();

		// X will be either 1 or size - 2
		if(rand.nextBoolean())
		{
			if(rand.nextBoolean())
				firstCell.x = size - 2;

			firstCell.y = rand.nextInt(size - 2) + 1;
			if(firstCell.y % 2 == 0)
				firstCell.y -= 1;
		}
		// Y will be either 0 or size - 1
		else
		{
			if(rand.nextBoolean())
				firstCell.y = size - 2;

			firstCell.x = rand.nextInt(size - 2) + 1;
			if(firstCell.x % 2 == 0)
				firstCell.x -= 1;
		}


		return firstCell;
	}

	private enum Dir
	{
		N(0, -1),
		S(0, 1),
		E(1, 0),
		W(-1, 0);

		final int dx;
		final int dy;

		private Dir(int dx, int dy)
		{
			this.dx = dx;
			this.dy = dy;
		}
	}
}
