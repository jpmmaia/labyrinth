package logic;

import random.IRandom;

/**
 * DragonMode2.java - This kind of dragon moves randomly.
 * 
 * @author Jo�o Maia
 *
 */
public class DragonMode2 extends Dragon
{
	private static final long serialVersionUID = 1L;
	protected IRandom random;
	
	/**
	 * The DragonMode2's Constructor.
	 * 
	 * @param random	an object which implements IRandom which generates random moves
	 * @see Dragon#Dragon(Coord, Labyrinth)
	 */
	public DragonMode2(Coord position, IRandom random, Labyrinth labyrinth)
	{
		super(position, labyrinth);
		
		this.random = random;
	}
	
	/**
	 * @see Dragon#Dragon(Coord, Labyrinth)
	 */
	public void update()
	{
		move();
	}
	
	/**
	 * Moves the dragon randomly
	 * 
	 * @see Dragon#move()
	 */
	protected void move()
	{
		Coord dragonDirection = random.nextDirection();

		if(labyrinth.isExit(this, dragonDirection))
			return;

		super.move(dragonDirection);
	}
	
	/**
	 * This kind of dragon is always awake.
	 * 
	 * @see Dragon#isSleeping()
	 */
	public boolean isSleeping()
	{
		return false;
	}
}
