package logic;

import java.awt.Point;

/**
 * Coord.java - Represents a coordinates system in the labyrinth.
 * <p>
 * It can be used as a vector, as it can be normalized (useful in the eagle implementation).
 * It can check if the coordinates are null. It has a toString() override so that it can be
 * easily used for debugging purposes.
 * <p>
 * It has also static functions which return the East, North, West and South directions.
 * 
 * @author Jo�o
 *
 */
public class Coord extends Point
{	
	private static final long serialVersionUID = 1L;
	
	/**
	 * The Coord's constructor.
	 * 
	 * @see java.awt.Point#Point()
	 */
	public Coord()
	{
		super();
	}
	
	/**
	 * The Coord's constructor which is initialized with x and y values.
	 * 
	 * @param x	the x value
	 * @param y	the y value
	 * 
	 * @see java.awt.Point#Point(int, int)
	 */
	public Coord(int x, int y)
	{
		super(x, y);
	}
	
	/**
	 * Normalizes the coordinates, as if the Coord was a vector
	 */
	public void normalize()
	{
		if(x == 0 && y == 0)
			return;
		
		x /= Math.round(Math.sqrt(x*x + y*y));
		y /= Math.round(Math.sqrt(x*x + y*y));
	}
	
	/**
	 * Copies the x and y value to a new Coord object
	 * 
	 * @return	a copy of the Coord object
	 */
	public Coord clone()
	{
		Coord coord = new Coord();
		
		coord.x = x;
		coord.y = y;
		
		return coord;
	}
	
	/**
	 * Prints the Coord in a nice and friendly way.
	 * <p>
	 * Used as debugging purposes.
	 * 
	 * @see Object#toString()
	 */
	public String toString()
	{
		return "[" + String.valueOf(x) + ", " + String.valueOf(y) + "] "; 
	}
	
	/**
	 * Checks the both Coord refer to the same position.
	 * 
	 * @param coord	the other Coord to be converted
	 * @return		true if both Coord x and y are equal
	 */
	public boolean equals(Coord coord)
	{
		return coord.x == x && coord.y == y;
	}
	
	/**
	 * 
	 * @return	true if x and y are equal to 0
	 */
	public boolean isNull()
	{
		return x == 0 && y == 0;
	}
	
	/**
	 * 
	 * @return	a Coord with x value equals to 1 and y equals to 0
	 */
	public static Coord east()
	{
		return new Coord(1, 0);
	}
	
	/**
	 * 
	 * @return	a Coord with x value equals to -1 and y equals to 0
	 */
	public static Coord west()
	{
		return new Coord(-1, 0);
	}
	
	/**
	 * 
	 * @return	a Coord with x value equals to 0 and y equals to -1
	 */
	public static Coord north()
	{
		return new Coord(0, -1);
	}
	
	/**
	 * 
	 * @return	a Coord with x value equals to 0 and y equals to 1
	 */
	public static Coord south()
	{
		return new Coord(0, 1);
	}
}
