package logic;

/**
 * Dragon.java - Abstract dragon class to handler all the dragon's common behavior.
 * 
 * @author Jo�o Maia
 *
 */
public abstract class Dragon extends Collider
{	
	private static final long serialVersionUID = 1L;
	
	private boolean sword = false;
	private boolean eagle = false;
	
	/**
	 * The Dragon's Constructor.
	 * 
	 * @param position
	 * @param labyrinth
	 * 
	 * @see Collider#Collider(Coord, char, Labyrinth)
	 */
	public Dragon(Coord position, Labyrinth labyrinth)
	{
		super(position, 'D', labyrinth);
	}

	/**
	 * @see Avatar#update()
	 */
	public abstract void update();
	
	/**
	 * The sleep behaviour is different for each type of dragon.
	 * 
	 * @return	true if the dragon is sleeping
	 */
	public abstract boolean isSleeping();
	
	/**
	 * Moves the dragon.
	 * <p>
	 * Depending on the dragon type, the move behavior will be different.
	 */
	protected abstract void move();
	
	
	/**
	 * Handles interaction with the hero, sword and eagle.
	 * 
	 * @see Collider#onCollisionEnter(Collider)
	 */
	public void onCollisionEnter(Collider other)
	{
		if(other instanceof Hero)
		{
			Hero hero = (Hero) other;
			onCollisionEnter(hero);
		}
		else if(other instanceof Sword)
		{
			Sword sword = (Sword) other;
			onCollisionEnter(sword);
		}
		else if(other instanceof Eagle)
		{
			Eagle eagle = (Eagle) other;
			onCollisionEnter(eagle);
		}
	}
	

	/**
	 * Handles the collision out event with the sword and eagle
	 * 
	 * @see Collider#onCollisionOut(Collider)
	 */
	public void onCollisionOut(Collider other)
	{
		if(other instanceof Sword)
			this.sword = false;
		else if(other instanceof Eagle)
			this.eagle = false;
	}
	
	/**
	 * Handles adjacent collision with the hero and eagle
	 * 
	 * @see Collider#onAdjacentCollisionEnter(Collider)
	 */
	public void onAdjacentCollisionEnter(Collider other)
	{
		if(other instanceof Hero || other instanceof Eagle)
			onCollisionEnter(other);
	}
	
	/**
	 * @see Avatar#getAvatar()
	 */
	public char getAvatar()
	{
		char avatar;
		
		if(eagle && sword)
			avatar = 'Q';
		else if(eagle)
			avatar = 'G';
		else if(sword)
			avatar = 'F';
		else
			avatar = 'D';
		
		if(isSleeping())
			avatar = Character.toLowerCase(avatar);
		
		return avatar;
	}
	
	
	private void onCollisionEnter(Hero hero)
	{
		if(!hero.gotSword())
			hero.kill();
	}
	private void onCollisionEnter(Sword sword)
	{
		this.sword = true;
	}
	private void onCollisionEnter(Eagle eagle)
	{	
		// Kill the eagle if it is not flying
		if(!eagle.isFlying() || eagle.isGrabbing())
		{
			eagle.kill();
			this.eagle = false;
			return;
		}
		
		if(eagle.getPosition().equals(position))
			this.eagle = true;
	}
}
