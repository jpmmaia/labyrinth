package logic;

/**
 * Sword.java - Represents the Sword in the labyrinth.
 * <p>
 * It can be grabbed by the hero. This class handles the
 * interaction with the other in-game objects.
 * 
 * @author Jo�o Maia
 */
public class Sword extends Collider
{	
	private static final long serialVersionUID = 1L;
	
	private Hero hero = null;
	private boolean eagle = false;
	private Dragon dragon = null;

	/**
	 * The Sword's constructor 
	 * 
	 * @see Collider#Collider(Coord, char, Labyrinth)
	 */
	public Sword(Coord position, Labyrinth labyrinth)
	{	
		super(position, 'E', labyrinth);
	}

	/**
	 * If the hero has grabbed the sword, then its position is the same
	 * as the hero's.
	 * 
	 * @see logic.Avatar#update()
	 */
	public void update()
	{
		if(hero != null)
			setPosition(hero.getPosition());
	}

	/**
	 * Handles the collision with the hero, eagle and dragon.
	 *
	 * @see logic.Collider#onCollisionEnter(logic.Collider)
	 */
	public void onCollisionEnter(Collider other)
	{
		if(other instanceof Hero)
		{
			Hero hero = (Hero) other;

			this.hero = hero;	
		}
		else if(other instanceof Eagle)
		{	
			Eagle eagle = (Eagle) other;
			if(eagle.isDead())
			{
				this.eagle = false;
				return;
			}

			this.eagle = true;
		}
		else if(other instanceof Dragon)
			dragon = (Dragon) other;
	}


	/**
	 * Handles the situation when the dragon and eagle no longer
	 * collide with the sword.
	 * 
	 * @see Collider#onCollisionOut(Collider)
	 */
	public void onCollisionOut(Collider other)
	{
		if(other instanceof Dragon)
			dragon = null;
		else if(other instanceof Eagle)
			eagle = false;
	}

	/**
	 * @return	true if the hero or eagle grabbed the sword
	 */
	public boolean wasGrabbed()
	{
		return hero != null || eagle;
	}
	
	/**
	 * @return 	the different avatars which the sword can have (when overlapped)
	 * 
	 * @see logic.Avatar#getAvatar()
	 */
	public char getAvatar()
	{
		char avatar;
		
		if(hero != null)
			avatar = 'A';
		else if(eagle && dragon != null)
			avatar = 'Q';
		else if(eagle)
			avatar = 'V';
		else if(dragon != null)
			avatar = 'F';
		else
			avatar = 'E';
		
		if(dragon != null && dragon.isSleeping())
			avatar = Character.toLowerCase(avatar);
		
		return avatar;
	}
}
