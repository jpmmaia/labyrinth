package logic;

import java.io.Serializable;

/**
 * Labyrinth.java - Keeps all the information about the labyrinth.
 * <p>
 * It has a copy of the map (only walls, grass and exit). The avatars 
 * need to notify the labyrinth of its position, so that the labyrinth
 * can display them.
 * 
 * @author Jo�o Maia
 */
public class Labyrinth implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private char [] [] map;
	private char [] [] buffer;
	private Coord exit;
	private int width;
	private int height;
	
	/**
	 * A Labyrinth's empty constructor. 
	 * 
	 */
	public Labyrinth()
	{
	}

	/**
	 * The Labyrinth's default constructor
	 * <p>
	 * Initializes the labyrinth with all the information it needs.
	 * 
	 * @param map	the matrix which contains the terrain information (including exit)
	 * @param exit	the coordinates of the exit
	 */
	public Labyrinth(char [][] map, Coord exit)
	{
		this.map = map;
		this.exit = exit;
		this.width = map.length;
		this.height = map[0].length;

		buffer = new char[map.length][map[0].length];
		clearBuffer();
	}

	/**
	 * This method is used to notify the position of an avatar.
	 * <p>
	 * If the avatar is dead, then it will not be displayed.
	 * 
	 * @param avatar	the avatar which notified the labyrinth.
	 */
	public void updateAvatar(Avatar avatar)
	{
		if(!avatar.isDead())
			setBufferSquare(avatar.getPosition(), avatar.getAvatar());
	}

	/**
	 * Checks if the avatar can move into a certain position.
	 * 
	 * @param avatar	the avatar which wants to change its position
	 * @param direction	the direction which the avatar wants to move
	 * @return			true if there isn't any wall on that position
	 */
	public boolean isValidDirection(Avatar avatar, Coord direction)
	{
		Coord position = avatar.getPosition().clone();

		position.translate(direction.x, direction.y);

		if(getMapSquare(position) == 'X')
			return false;

		return true;
	}

	/**
	 * Checks if a certaint position is the exit.
	 * 
	 * @param avatar	the avatar which wants to move
	 * @param direction	the direction in which the avatar wants to move
	 * @return			true if the position is the exit
	 */
	public boolean isExit(Avatar avatar, Coord direction)
	{
		Coord position = avatar.getPosition().clone();

		position.translate(direction.x, direction.y);

		if(getMapSquare(position) != 'S')
			return false;

		return true;
	}

	/**
	 * Used to print the labyrinth
	 * 
	 * @see Object#toString()
	 */
	public String toString()
	{
		String maze = "";

		for(char[] line : buffer)
			maze += String.copyValueOf(line) + '\n';
			

		return maze;
	}

	/**
	 * Erases all the avatars present in the labyrinth
	 */
	public void clearBuffer()
	{
		for(int i = 0; i < map.length; i++)
			for(int j = 0; j < map[0].length; j++)
				buffer[i][j] = map[i][j];
	}
	
	/**
	 * 
	 * @param avatar	the avatar
	 * @param position	the square position
	 * @return			true if the labyrinth square corresponds to the avatar
	 */
	public boolean isAvatar(Avatar avatar, Coord position)
	{
		return buffer[position.y][position.x] == avatar.getAvatar();
	}
	
	/**
	 * Copies all the information of the other labyrinth.
	 * 
	 * @param other	the labyrinth to be copied
	 */
	public void clone(Labyrinth other)
	{
		map = other.getMap();
		exit = other.getExit();
		width = other.getWidth();
		height = other.getHeight();
		buffer = new char[height][width];
		
		clearBuffer();
	}

	/**
	 * 
	 * @return	the coordinates of the exit
	 */
	public Coord getExit()
	{
		return exit;
	}

	/**
	 * 
	 * @param position	the position of the square
	 * @return			the character of the square (including avatars)
	 */
	public char getBufferSquare(Coord position)
	{
		return buffer[position.y][position.x];
	}

	/**
	 * 
	 * @return	the width of the labyrinth
	 */
	public int getWidth()
	{
		return width;
	}
	
	/**
	 * 
	 * @return	the height of the labyrinth
	 */
	public int getHeight()
	{
		return height;
	}
	
	/**
	 * 
	 * @return	the terrain of the labyrinth (walls, grass and exit)
	 */
	public char [][] getMap()
	{
		return map;
	}
	
	/**
	 * Sets the terrain of the labyrinth. It changes the width and height of the labyrinth.
	 * 
	 * @param map	the terrain to be set
	 */
	public void setMap(char[][] map)
	{
		this.map = map;
		width = map[0].length;
		height = map.length;
		
		buffer = new char[height][width];
	}
	
	/**
	 * Sets the coordinates of the exit of the labyrinth.
	 * 
	 * @param exit	the coordinates of the exit
	 */
	public void setExit(Coord exit)
	{
		this.exit = exit;
	}

	
	private void setBufferSquare(Coord position, char object)
	{
		buffer[position.y][position.x] = object;
	}
	
	private char getMapSquare(Coord position)
	{
		return map[position.y][position.x];
	}
}
