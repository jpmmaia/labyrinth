package logic;


/**
 * Hero.java - Represents the hero of the labyrinth.
 * <p>
 * It's commanded by the user. It can interact with other colliders
 * as it is a collider.
 * 
 * @author Jo�o Maia
 */
public class Hero extends Collider
{
	private static final long serialVersionUID = 1L;
	private Sword sword = null;
	
	/**
	 * The hero's constructor.
	 * 
	 * @see Collider#Collider(Coord, char, Labyrinth)
	 */
	public Hero(Coord position, Labyrinth labyrinth)
	{
		super(position, 'H', labyrinth);
	}

	/**
	 * @see logic.Avatar#update()
	 */
	public void update()
	{
	}

	/**
	 * In addition, it checks if the hero can reach the exit.
	 * 
	 * @see logic.Collider#move(logic.Coord)
	 */
	public void move(Coord direction)
	{	
		if(labyrinth.isExit(this, direction) && sword == null)
			return;
		
		super.move(direction);
	}

	/**
	 * Hero interaction with other colliders.
	 * <p>
	 * Responsible for killing the dragon, grabbing the sword
	 * and interacting with the eagle.
	 * 
	 * @see logic.Collider#onCollisionEnter(logic.Collider)
	 */
	public void onCollisionEnter(Collider other)
	{
		if(other instanceof Dragon && sword != null)
		{	
			Dragon dragon = (Dragon) other;
			dragon.kill();
		}
		else if(other instanceof Sword)
		{
			Sword sword = (Sword) other;
			this.sword = sword;

			setAvatar('A');
		}
		else if(other instanceof Eagle)
		{
			Eagle eagle = (Eagle) other;
			if(!eagle.isFlying())
				eagle.setPosition(position);
		}
	}

	/**
	 * 
	 * @see logic.Collider#onCollisionOut(logic.Collider)
	 */
	public void onCollisionOut(Collider other)
	{
		if(other instanceof Sword)
		{
			this.sword = null;
			setAvatar('H');
		}
	}

	/**
	 * @return	true if the hero has grabbed the sword
	 */
	public boolean gotSword()
	{
		return sword != null;
	}
}
