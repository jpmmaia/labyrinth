package logic;

import java.io.Serializable;

/**
 * Avatar.java - Represents any object in the maze.
 * <p>
 * It represents any object which can be moved through the
 * labyrinth. It has also an avatar which is a character used
 * to represent the specific object in-game (i.e. 'H' to 
 * represent an Hero). It has also a state to indicate if the
 * object is dead or not (in scene or not).
 * 
 * @author Jo�o Maia
 */
public abstract class Avatar implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * It's used to tell the labyrinth where the avatar
	 * currently is. Whenever the avatar moves, it must
	 * notify the labyrinth.
	 */
	protected Labyrinth labyrinth;
	
	/**
	 * It represents the position of the avatar in the 
	 * labyrinth.
	 */
	protected Coord position;
	
	/**
	 * A character to represent the object in the
	 * labyrinth.
	 */
	protected char avatar;
	
	/**
	 * A state that represents if the object is still
	 * in the game, or not (i.e. a Dragon may die).
	 */
	protected boolean dead = false;

	/**
	 * The avatar constructor.
	 * <p>
	 * Initializes the avatar with its position, the char which
	 * represents it and the labyrinth.
	 * 
	 * @param position	the avatar's position
	 * @param avatar	a char used to represent the object in the labyrinth
	 * @param labyrinth	the avatar's labyrinth
	 */
	public Avatar(Coord position, char avatar, Labyrinth labyrinth)
	{
		this.position = position;
		this.avatar = avatar;
		this.labyrinth = labyrinth;
	}
	
	/**
	 * Called every time a move is done (every time the user makes a move)
	 * 
	 * @see GameLogic
	 */
	public abstract void update();

	/**
	 * Used to move the avatar
	 * 
	 * @param direction	the direction to which the avatar is been moved
	 * @see Coord
	 */
	protected void move(Coord direction)
	{
		// Calculating new avatar position
		position.translate(direction.x, direction.y);
	}

	/**
	 * @return	the character used to represent the object in the labyrinth
	 */
	public char getAvatar()
	{
		return avatar;
	}
	
	/**
	 * @param avatar	the character used to represent the object in the
	 * labyrinth 
	 */
	public void setAvatar(char avatar)
	{
		this.avatar = avatar;
	}
	
	/**
	 * @return	the current position of the object in the labyrinth
	 */
	public Coord getPosition()
	{
		return position;
	}
	
	/**
	 * @param position	the new object's position in the labyrinth
	 */
	public void setPosition(Coord position)
	{
		this.position = position;
	}
	
	/**
	 * @return	true if the object is dead
	 */
	public boolean isDead()
	{
		return dead;
	}
	
	/**
	 * It kills the object (i.e. the hero)
	 */
	public void kill()
	{
		dead = true;
	}
}
