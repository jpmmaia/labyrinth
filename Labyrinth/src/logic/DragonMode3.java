package logic;

import random.IRandom;

import java.lang.Character;

/**
 * DragomMode3.java - This kind of dragon moves randomly and sleeps once upon a time.
 * 
 * @author Jo�o Maia
 *
 */
public class DragonMode3 extends DragonMode2
{
	private static final long serialVersionUID = 1L;
	private boolean sleeping = false;

	/**
	 * The DragonMode3's Constructor.
	 * 
	 * @see DragonMode2#DragonMode2(Coord, IRandom, Labyrinth)
	 */
	public DragonMode3(Coord position, IRandom random, Labyrinth labyrinth)
	{
		super(position, random, labyrinth);
	}

	/**
	 * Moves the dragon and determines if the dragon falls asleep.
	 * 
	 * @see Dragon#update()
	 */
	public void update()
	{
		// 33% chance of awaking and 33% chance of falling asleep
		boolean sleepProb = sleeping ? !random.dragonSleepProb() : random.dragonSleepProb();

		sleepBehaviour(sleepProb);

		move();
	}

	/**
	 * Moves the dragon if he's not sleeping.
	 * 
	 * @see Dragon#move()
	 */
	protected void move()
	{
		if(!sleeping)
			super.move();
	}
	
	/**
	 * This kind of dragon may be sleeping.
	 * 
	 * @see Dragon#isSleeping()
	 */
	public boolean isSleeping()
	{
		return sleeping;
	}

	private void sleepBehaviour(boolean sleep)
	{
		if(sleep && !sleeping)
			sleep();
		else if(sleeping)
			awake();
	}

	private void sleep()
	{
		sleeping = true;

		// Changing to lower case avatar
		setAvatar(Character.toLowerCase(getAvatar()));
	}

	private void awake()
	{
		sleeping = false;

		// Changing to upper case avatar
		setAvatar(Character.toUpperCase(getAvatar()));
	}
}
