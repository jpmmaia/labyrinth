package logic;

/**
 * Eagle.java - The hero's eagle which can help him retrieving the sword.
 * <p>
 * It is responsible for the movement behaviour of the eagle and for
 * grabbing the sword. It handles interaction with the dragon too.
 * 
 * @author Jo�o Maia
 */
public class Eagle extends Collider
{
	private static final long serialVersionUID = 1L;
	
	private Sword sword;
	private Dragon dragon = null;
	private boolean grabSword = false;
	private boolean returning = false;
	private boolean flying = false;
	private boolean grabbing = false;
	private Coord landingPosition;

	/**
	 * The Eagle's Constructor
	 * 
	 * @param hero	the owner of the eagle
	 * @param sword	the sword of the labyrinth
	 * 
	 * @see Collider#Collider(Coord, char, Labyrinth)
	 */
	public Eagle(Hero hero, Sword sword, Labyrinth labyrinth)
	{
		super(hero.getPosition(), 'B', labyrinth);

		this.sword = sword;
	}

	/**
	 * It is responsible for moving the eagle when she is flying.
	 * 
	 * @see Avatar#update()
	 */
	public void update()
	{
		if(!flying || dead)
			return;

		if(grabbing)
			grabbing = false;

		if(sword.wasGrabbed())
			returning = true;

		if(!returning)
			flyTo();
		else
			flyFrom();

		detectCollision();
	}

	private void detectCollision()
	{
		if(sword.getPosition().equals(position))
			onCollisionEnter(sword);
	}

	private void onCollisionEnter(Sword sword) 
	{
		if(grabSword)
			return;

		sword.setPosition(position);

		grabSword = true;
		returning = true;
		grabbing = true;
	}

	/**
	 * 
	 * @see logic.Avatar#kill()
	 */
	public void kill()
	{
		dead = true;
	}

	/**
	 * @return	true if the eagle has grabbed the sword
	 */
	public boolean gotSword()
	{
		return grabSword;
	}

	/**
	 * 
	 * @return	true if the eagle is flying
	 */
	public boolean isFlying()
	{
		return flying;
	}

	/**
	 * It is called when the user command the eagle to fly.
	 */
	public void fly()
	{
		if(flying)
			return;

		landingPosition = position.clone();
		position = position.clone();
		flying = true;
		returning = false;
	}

	private void flyTo()
	{
		Coord direction = sword.getPosition().clone();
		direction.translate(-position.x, -position.y);

		direction.normalize();

		position.translate(direction.x, direction.y);
	}

	private void flyFrom()
	{
		Coord direction = landingPosition.clone();
		direction.translate(-position.x, -position.y);

		direction.normalize();

		position.translate(direction.x, direction.y);

		if(position.equals(landingPosition))
			land();
	}

	private void land()
	{
		flying = false;
		returning = false;
	}

	/**
	 * 
	 * @see Collider#onCollisionEnter(Collider)
	 */
	public void onCollisionEnter(Collider other) 
	{
		if(other instanceof Dragon)
			dragon = (Dragon) other;
	}

	/**
	 * 
	 * @see Collider#onCollisionOut(Collider)
	 */
	public void onCollisionOut(Collider other) 
	{
		if(other instanceof Dragon)
		{
			Dragon dragon = (Dragon) other;
			if(dragon == this.dragon)
				this.dragon = null;
		}
	}

	/**
	 * The eagle doesn't interact with other colliders on adjacent
	 * cells.
	 * 
	 * @see Collider#onAdjacentCollisionEnter(Collider)
	 */
	public void onAdjacentCollisionEnter(Collider other)
	{
	}

	/**
	 * 
	 * @return	true if the eagle is grabbing the sword
	 */
	public boolean isGrabbing()
	{
		return grabbing;
	}
	
	/**
	 * 
	 * @see Avatar#getAvatar()
	 */
	public char getAvatar()
	{
		char avatar;
		
		if(grabSword && dragon != null)
			avatar = 'Q';
		else if(grabSword)
			avatar = 'V';
		else if(dragon != null)
			avatar = 'G';
		else
			avatar = 'B';
		
		if(dragon != null)
			if(dragon.isSleeping())
				avatar = super.getAvatar();
		
		return avatar;
	}
}
