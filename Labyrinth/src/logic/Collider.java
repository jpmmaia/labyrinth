package logic;

/**
 * Collider.java - Represents an avatar which collides
 * <p>
 * An object which collides with another objects can be
 * used as a trigger to some events. For instance, it can
 * be used to detect when the hero is next to a dragon, or
 * the sword is caught by the hero.
 * 
 * @author Jo�o Maia
 */
public abstract class Collider extends Avatar
{
	private static final long serialVersionUID = 1L;
	
	private Collider other = null;

	/**
	 * Collider's constructor.
	 * 
	 * @see Avatar#Avatar(Coord, char, Labyrinth)
	 */
	public Collider(Coord position, char avatar, Labyrinth labyrinth)
	{
		super(position, avatar, labyrinth);
	}

	/**
	 * In addition to moving the object, it also checks if there is a wall
	 * in that direction. If there is, then the object is not moved.
	 * 
	 * @see logic.Avatar#move(logic.Coord)
	 */
	public void move(Coord direction)
	{	
		// Checking if there is a wall
		if(!labyrinth.isValidDirection(this, direction))
			return;

		super.move(direction);
	}
	
	/**
	 * The method which is called when a collider is the exact same
	 * position as this collider.
	 * 
	 * @param other	the other collider
	 */
	public abstract void onCollisionEnter(Collider other);
	
	/**
	 * The method which is called when a collider is no longer colliding
	 * with this collider.
	 * 
	 * @param other	the other collider
	 */
	public abstract void onCollisionOut(Collider other);

	/**
	 * Checks if the collider is in the exact same position
	 * as the other collider. If it is then the onCollisionEnter 
	 * method of both colliders are called.
	 * It also detects the onCollisionOut event.
	 * 
	 * @param other	the other collider which is checked against the current
	 * @see Collider#onCollisionEnter(Collider)
	 * @see Collider#onCollisionOut(Collider)
	 */
	public void detectCollision(Collider other)
	{
		if(position.equals(other.getPosition()))
		{
			this.other = other;
			other.onCollisionEnter(this);
			this.onCollisionEnter(other);
		} 
		// If no collision is detected and it is the same collider
		else if(this.other == other)
		{
			other.onCollisionOut(this);
			onCollisionOut(other);
			this.other = null;
		}
	}

	/**
	 * Detects if the collider other is in an adjacent position
	 * 
	 * @param other	the collider which is checked againts the current
	 */
	public void detectAdjacentCollision(Collider other)
	{
		Coord [] directions = { Coord.east(), Coord.north(), Coord.west(), Coord.south() };

		Coord otherPosition = other.getPosition();

		for(int i = 0; i < directions.length; i++)
		{
			Coord neigh = this.position.clone();
			neigh.translate(directions[i].x, directions[i].y);

			if(otherPosition.equals(neigh))
			{
				other.onAdjacentCollisionEnter(this);
				this.onAdjacentCollisionEnter(other);
			}
		}
	}
	
	/**
	 * The method which is called when collider is detected on an adjacent position.
	 * <p>
	 * By default, it calls {@link #onCollisionEnter(Collider)}
	 * 
	 * @param other	the collider which is checked againts the current
	 */
	public void onAdjacentCollisionEnter(Collider other)
	{
		onCollisionEnter(other);
	}
}
