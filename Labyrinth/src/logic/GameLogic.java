package logic;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Iterator;

/**
 * GameLogic.java - Runs the game.
 * <p>
 * It has states to check if game has been won or lost by the hero and 
 * the dragon's mode. It is responsible for calling all the avatar's
 * update and collision methods. It also updates the labyrinth.
 * 
 * @author Jo�o Maia
 */
public class GameLogic implements Serializable
{
	/**
	 * Used to represent if the game has been won or lost.
	 */
	public enum state
	{
		playing,
		won,
		lost,
	}

	/**
	 * Used to check the dragon's mode
	 */
	public enum mode
	{
		freeze,
		random,
		randomSleep
	}

	/**
	 * Used to run the game
	 * 
	 * @see GameLogic#run(event)
	 */
	public enum event
	{
		moveEast,
		moveNorth,
		moveWest,
		moveSouth,
		fly, 
		none
	}

	private static final long serialVersionUID = 1L;
	private Labyrinth labyrinth;
	private Hero hero;
	private Sword sword;
	private Eagle eagle;
	private LinkedList<Dragon> dragons = new LinkedList<Dragon>();
	private state gameState = state.playing;


	/**
	 * The GameLogic's Constructor
	 * 
	 * @param labyrinth	the labyrinth's game
	 * @param hero		the hero
	 * @param eagle		the hero's eagle
	 * @param sword		the sword
	 * @param dragons	an array of dragons
	 */
	public GameLogic(Labyrinth labyrinth, Hero hero, Eagle eagle, Sword sword, Dragon [] dragons)
	{
		this.labyrinth = labyrinth;
		this.hero = hero;
		this.eagle = eagle;
		this.sword = sword;

		for(int i = 0; i < dragons.length; i++)
			this.dragons.add(dragons[i]);
	}

	/**
	 * Runs the game.
	 * <p>
	 * Firstly, it calls the update method of all the avatars in-game.
	 * Secondly, it detects all the avatars which are in adjacent positions.
	 * Thirdly, it detects all the avatars which are in the same position.
	 * Fourthly, it checks if the game has been won or lost, and if any
	 * dragon or the eagle are dead.
	 * Finally, it notifies the labyrinth with all avatar positions.
	 * 
	 * @param e	the next move (depending on the user input)
	 * 
	 * @see GameLogic.event
	 */
	public void run(event e)
	{
		labyrinth.clearBuffer();

		update(e);
		detectAdjacentCollisions();
		detectCollisions();

		detectChanges();

		updateLabyrinth();
	}

	/**
	 * Copies all the information from the other gameLogic this one.
	 * <p>
	 * Very useful to replace an object when it is referenced multiple times.
	 * 
	 * @param other	the object to be clone
	 */
	public void clone(GameLogic other)
	{
		this.labyrinth.clone(other.getLabyrinth());
		this.hero = other.getHero();
		this.sword = other.getSword();
		this.dragons = other.getDragons();
		this.eagle = other.getEagle();
		this.gameState = other.getState();

		updateLabyrinth();
	}

	/**
	 * 
	 * @return	the labyrinth.
	 */
	public Labyrinth getLabyrinth()
	{
		return labyrinth;
	}

	/**
	 * 
	 * @return	the state of the labyrinth.
	 * 
	 * @see GameLogic.state
	 */
	public state getState()
	{
		return gameState;
	}

	/**
	 * 
	 * @return	the hero.
	 */
	public Hero getHero()
	{
		return hero;
	}

	/**
	 * 
	 * @return 	the hero's eagle.
	 */
	public Eagle getEagle()
	{
		return eagle;
	}

	/**
	 * 
	 * @return	the sword.
	 */
	public Sword getSword()
	{
		return sword;
	}

	/**
	 * 
	 * @return 	a list of all the dragons which are alive.
	 */
	public LinkedList<Dragon> getDragons()
	{
		return dragons;
	}

	/**
	 * It returns the corresponding event to the direction.
	 * <p>
	 * The direction must correspond to the East, North, West
	 * or South directions. Otherwise, it will return the
	 * event 'none'.
	 * 
	 * @param direction	the vector of the direction
	 * @return			the event which corresponds to the direction
	 * @see Coord#east()
	 * @see Coord#north()
	 * @see Coord#west()
	 * @see Coord#south()
	 * @see GameLogic.event
	 */
	public static GameLogic.event getEvent(Coord direction)
	{ 
		if(direction.equals(Coord.east()))
			return GameLogic.event.moveEast;

		if(direction.equals(Coord.north()))
			return GameLogic.event.moveNorth;

		if(direction.equals(Coord.west()))
			return GameLogic.event.moveWest;

		if(direction.equals(Coord.south()))
			return GameLogic.event.moveSouth;

		return GameLogic.event.none;
	}

	public void update(event e)
	{	
		switch(e)
		{
		case moveEast:
			hero.move(Coord.east());
			break;
		case moveNorth:
			hero.move(Coord.north());
			break;
		case moveWest:
			hero.move(Coord.west());
			break;
		case moveSouth:
			hero.move(Coord.south());
			break;
		case fly:
			eagle.fly();
			break;
		case none:
			break;
		}

		sword.update();
		if(eagle != null)
			eagle.update();

		Iterator<Dragon> dragonsIt = dragons.listIterator();
		while(dragonsIt.hasNext())
			dragonsIt.next().update();
	}


	private void detectCollisions()
	{
		hero.detectCollision(sword);
		if(eagle != null)
		{
			hero.detectCollision(eagle);
			eagle.detectCollision(sword);
		}

		Iterator<Dragon> dragonsIt = dragons.listIterator();
		while(dragonsIt.hasNext())
		{
			Dragon dragon = dragonsIt.next();
			hero.detectCollision(dragon);
			sword.detectCollision(dragon);

			if(eagle != null)
				dragon.detectCollision(eagle);
		}
	}

	private void detectAdjacentCollisions()
	{	
		Iterator<Dragon> dragonsIt = dragons.listIterator();
		while(dragonsIt.hasNext())
		{
			Dragon dragon = dragonsIt.next();

			hero.detectAdjacentCollision(dragon);

			if(eagle != null)
				dragon.detectAdjacentCollision(eagle);
		}
	}

	private void detectChanges()
	{
		if(hero.isDead())
			gameState = state.lost;
		else if(labyrinth.getExit().equals(hero.getPosition()))
			gameState = state.won;

		if(eagle != null)
			if(eagle.isDead())
				eagle = null;

		Iterator<Dragon> dragonsIt = dragons.listIterator();
		while(dragonsIt.hasNext())
			if(dragonsIt.next().isDead())
				dragonsIt.remove();
	}

	private void updateLabyrinth()
	{
		if(eagle != null)
			labyrinth.updateAvatar(eagle);

		labyrinth.updateAvatar(hero);
		labyrinth.updateAvatar(sword);
		Iterator<Dragon> dragonsIt = dragons.listIterator();
		while(dragonsIt.hasNext())
			labyrinth.updateAvatar(dragonsIt.next());
	}
}
