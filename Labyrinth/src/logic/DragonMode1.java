package logic;

/**
 * DragonMode1.java - The is the dragon which cannot move and is always awake.
 * 
 * @author Jo�o Maia
 *
 */
public class DragonMode1 extends Dragon
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * The DragonMode1's Constructor.
	 * 
	 * @see Dragon#Dragon(Coord, Labyrinth)
	 */
	public DragonMode1(Coord position, Labyrinth labyrinth)
	{
		super(position, labyrinth);
	}
	
	/**
	 * 
	 * @see Avatar#update()
	 */
	public void update()
	{
		move();
	}
	
	/**
	 * The dragon doesn't move in this mode.
	 * 
	 * @see Dragon#move()
	 */
	protected void move()
	{
		// Nothing to do
	}
	
	/**
	 * This dragon is always awake.
	 * 
	 * @see Dragon#isSleeping()
	 */
	public boolean isSleeping()
	{
		return false;
	}
}
