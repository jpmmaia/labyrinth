package testing;
import static org.junit.Assert.*;

import org.junit.Test;

import java.util.LinkedList;

import builder.IMazeBuilder;
import builder.DefaultMazeBuilder;
import logic.*;
import random.TestLogic;

/**
 * BaseTester.java - Use to test the most basic functionalities of the game.
 * 
 * @author Jo�o Maia
 */
public class BaseTester 
{
	private GameLogic gameLogic;
	private Hero hero;
	private Sword sword;
	private Labyrinth labyrinth;

	public BaseTester()
	{
		IMazeBuilder builder = new DefaultMazeBuilder();

		gameLogic = builder.build(GameLogic.mode.freeze, new TestLogic(), 0, 1);
		hero = gameLogic.getHero();
		labyrinth = gameLogic.getLabyrinth();
		sword = gameLogic.getSword();
	}

	@Test
	public void simpleMove() 
	{
		Coord direction = Coord.east();
		Coord currentPosition = hero.getPosition().clone();
		Coord initialPosition = currentPosition.clone();

		update(direction);
		currentPosition.translate(direction.x, direction.y);

		assertTrue(currentPosition.equals(hero.getPosition()));
		assertTrue(labyrinth.isAvatar(hero, currentPosition));
		assertTrue(labyrinth.getBufferSquare(initialPosition) == ' ');
	}

	@Test
	public void notmove()
	{
		Coord currentPosition = hero.getPosition().clone();
		Coord direction = Coord.west();

		update(direction);

		assertTrue(currentPosition.equals(hero.getPosition()));
		assertTrue(labyrinth.isAvatar(hero, currentPosition));
		
		currentPosition.translate(direction.x, direction.y);
		assertTrue(labyrinth.getBufferSquare(currentPosition) == 'X');
	}

	@Test
	public void grabSword()
	{
		update(Coord.east(), 3);
		update(Coord.south(), 4);
		update(Coord.west(), 3);
		update(Coord.south(), 3);

		assertTrue(hero.gotSword());
		assertTrue(hero.getAvatar() == 'A');
		assertTrue(hero.getPosition().equals(sword.getPosition()));
	}

	@Test
	public void KIA()
	{
		update(Coord.south());
		assertTrue(gameLogic.getState() == GameLogic.state.lost);
	}

	@Test
	public void killDragon()
	{	
		LinkedList<Dragon> dragons = gameLogic.getDragons();
		assertFalse(dragons.isEmpty());
		Dragon dragon = dragons.get(0);
		
		update(Coord.east(), 3);
		update(Coord.south(), 4);
		update(Coord.west(), 3);	
		update(Coord.south(), 3);	
		update(Coord.north(), 3);
		
		assertTrue(!dragon.isDead());
		assertTrue(hero.gotSword());
		update(Coord.north());
		
		assertTrue(dragon.isDead());
		assertTrue(dragons.isEmpty());
	}

	@Test
	public void exitWithSwordAndKill()
	{
		// Grab Sword
		update(Coord.east(), 3);
		update(Coord.south(), 4);
		update(Coord.west(), 3);
		update(Coord.south(), 3);

		// Kill & Initial Position
		update(Coord.north(), 7);

		// Exit
		update(Coord.east(), 7);
		update(Coord.south(), 4);
		update(Coord.east());

		assertTrue(hero.getPosition().equals(labyrinth.getExit()));
		assertTrue(gameLogic.getState() == GameLogic.state.won);
	}

	@Test
	public void exitWithoutSwordAndKill()
	{
		// Exit
		update(Coord.east(), 7);
		update(Coord.south(), 4);
		update(Coord.east());

		assertFalse(hero.getPosition().equals(labyrinth.getExit()));
		assertTrue(gameLogic.getState() == GameLogic.state.playing);
	}

	private void update(Coord direction)
	{
		GameLogic.event e = GameLogic.getEvent(direction);
		
		gameLogic.run(e);
	}
	private void update(Coord direction, int repeat)
	{
		GameLogic.event e = GameLogic.getEvent(direction);
		
		for(int i = 0; i < repeat; i++)
			gameLogic.run(e);
	}
}
