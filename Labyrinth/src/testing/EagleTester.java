package testing;

import static org.junit.Assert.*;

import org.junit.Test;

import random.TestLogic;
import builder.DefaultMazeBuilder;
import logic.Coord;
import logic.Eagle;
import logic.GameLogic;
import logic.Hero;
import logic.Labyrinth;
import logic.Sword;

/**
 * EagleTester.java - Used to test the eagle behavior and interaction with other objects.
 * 
 * @author Jo�o Maia
 */
public class EagleTester 
{
	GameLogic gameLogic;
	Labyrinth labyrinth;
	Eagle eagle;
	Sword sword;
	Hero hero;
	TestLogic testLogic;
	
	public EagleTester()
	{	
		DefaultMazeBuilder builder = new DefaultMazeBuilder();
		
		testLogic = new TestLogic();
		gameLogic = builder.build(GameLogic.mode.randomSleep, testLogic, 0, 1);
		
		labyrinth = gameLogic.getLabyrinth();
		eagle = gameLogic.getEagle();
		sword = gameLogic.getSword();
		hero = gameLogic.getHero();
	}
	
	public void run(GameLogic.event e)
	{
		gameLogic.run(e);
	}
	
	public void run(GameLogic.event e, int repeat)
	{
		for(int i = 0; i < repeat; i++)
			gameLogic.run(e);
	}
	
	private void moveDragon(Coord direction, int repeat)
	{
		testLogic.setNextDirection(direction);
		
		for(int i = 0; i < repeat; i++)
			gameLogic.run(GameLogic.event.none);
		
		testLogic.setNextDirection(new Coord(0, 0));
	}
	
	public void dragonSleep(boolean sleep)
	{
		testLogic.setDragonSleepBehaviour(sleep);
	}
	
	@Test
	public void simpleMove()
	{
		// Testing if initial conditions are correct
		assertFalse(eagle.gotSword());
		
		run(GameLogic.event.fly);
		
		// Testing position
		Coord position = new Coord(1, 2);
		assertTrue(eagle.getPosition().equals(position));
		assertTrue(labyrinth.getBufferSquare(position) == 'B');
		
		// Testing if dragon didn't kill the eagle
		assertFalse(eagle.isDead());
		
		// Testing if flying
		assertTrue(eagle.isFlying());
	}
	
	@Test
	public void dragonCollisionFlying()
	{
		run(GameLogic.event.fly, 1);
		assertTrue(labyrinth.getBufferSquare(new Coord(1, 3)) == 'D');
		
		run(GameLogic.event.fly, 1);
		
		// Testing position
		Coord position = new Coord(1, 3);
		assertTrue(eagle.getPosition().equals(position));
		
		// Testing if dragon didn't kill the dragon
		assertFalse(eagle.isDead());
		
		// Testing avatar
		assertTrue(labyrinth.getBufferSquare(position) == 'G');
		
		run(GameLogic.event.fly, 2);
		assertTrue(labyrinth.getBufferSquare(position) == 'D');
		
		assertTrue(labyrinth.getBufferSquare(new Coord(1, 5)) == 'B');
	}
	
	@Test
	public void grabSword()
	{	
		run(GameLogic.event.fly, 6);
		
		Coord position = eagle.getPosition();
		
		// Eagle
		assertTrue(labyrinth.getBufferSquare(position) == 'B');
		
		run(GameLogic.event.fly);
		
		// Eagle + Sword
		assertTrue(labyrinth.getBufferSquare(position) == 'V');
		// Did the eagle grab the sword?
		assertTrue(eagle.gotSword());
		
		run(GameLogic.event.fly);
		
		// Testing position and avatar
		assertTrue(eagle.getPosition().equals(new Coord(1, 7)));
		assertTrue(labyrinth.getBufferSquare(position) == 'V');
	}
	
	@Test
	public void returningWithSword()
	{
		run(GameLogic.event.fly, 11);
		
		Coord position = new Coord(1, 4);
		assertTrue(labyrinth.getBufferSquare(position) == 'V');
		
		run(GameLogic.event.fly);
		Coord north = Coord.north();
		position.translate(north.x, north.y);
		assertTrue(labyrinth.getBufferSquare(position) == 'Q');
		
		run(GameLogic.event.fly, 3);
		Coord initialPosition = new Coord(1, 1);
		assertTrue(labyrinth.getBufferSquare(initialPosition) == 'A');
		assertTrue(!eagle.isFlying());
		assertTrue(hero.gotSword());
	}
	
	@Test
	public void returningWithSword2()
	{
		run(GameLogic.event.fly, 11);
		
		Coord position = new Coord(1, 4);
		assertTrue(labyrinth.getBufferSquare(position) == 'V');
		
		run(GameLogic.event.fly);
		Coord north = Coord.north();
		position.translate(north.x, north.y);
		assertTrue(labyrinth.getBufferSquare(position) == 'Q');
		
		
		run(GameLogic.getEvent(Coord.east()), 5);
		Coord initialPosition = new Coord(1, 1);
		assertTrue(labyrinth.getBufferSquare(initialPosition) == 'V');
		assertTrue(!eagle.isFlying());
		
		// Dragon kill
		moveDragon(Coord.north(), 1);
		assertTrue(eagle.isDead());
		assertTrue(labyrinth.getBufferSquare(initialPosition) == 'E');
		
		moveDragon(Coord.north(), 1);
		assertTrue(labyrinth.getBufferSquare(initialPosition) == 'F');
	}
	
	@Test
	public void returningWithSword3()
	{
		run(GameLogic.event.fly, 11);
		
		Coord position = new Coord(1, 4);
		assertTrue(labyrinth.getBufferSquare(position) == 'V');
		
		run(GameLogic.event.fly);
		Coord north = Coord.north();
		position.translate(north.x, north.y);
		assertTrue(labyrinth.getBufferSquare(position) == 'Q');
		
		
		run(GameLogic.getEvent(Coord.east()), 5);
		Coord initialPosition = new Coord(1, 1);
		assertTrue(labyrinth.getBufferSquare(initialPosition) == 'V');
		assertTrue(!eagle.isFlying());
		
		// Hero sword grab
		run(GameLogic.getEvent(Coord.west()), 5);
		assertTrue(hero.gotSword());
		assertTrue(labyrinth.getBufferSquare(initialPosition) == 'A');
	}
	
	@Test
	public void killEagle1()
	{
		moveDragon(Coord.south(), 4);
		
		run(GameLogic.event.fly, 7);
		assertTrue(eagle.isDead());
		assertTrue(labyrinth.getBufferSquare(new Coord(1, 8)) == 'E');
	}
	
	@Test
	public void killEagle2()
	{
		moveDragon(Coord.south(), 5);
		
		run(GameLogic.event.fly, 7);
		assertTrue(eagle.isDead());
		assertTrue(labyrinth.getBufferSquare(new Coord(1, 8)) == 'F');
	}
}
