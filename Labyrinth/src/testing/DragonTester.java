package testing;

import static org.junit.Assert.*;
import logic.Coord;
import logic.Dragon;
import logic.GameLogic;
import logic.Labyrinth;
import builder.DefaultMazeBuilder;
import builder.IMazeBuilder;
import random.TestLogic;

import org.junit.Test;

import java.util.LinkedList;

/**
 * DragonTeste.java - Used to test the dragon behavior and interaction with other objects.
 * 
 * @author Jo�o Maia
 */
public class DragonTester 
{
	private GameLogic gameLogic;
	private LinkedList<Dragon> dragons;
	private Labyrinth labyrinth;
	

	private TestLogic testLogic = new TestLogic();
	
	private void initialize(GameLogic.mode gameMode)
	{
		IMazeBuilder builder = new DefaultMazeBuilder();
		
		gameLogic = builder.build(gameMode, testLogic, 0, 1);

		dragons = gameLogic.getDragons();
		labyrinth = gameLogic.getLabyrinth();
	}
	
	private void moveDragon(Coord direction)
	{
		testLogic.setNextDirection(direction);
		gameLogic.run(GameLogic.event.none);
	}
	
	private void moveDragon(Coord direction, int repeat)
	{
		testLogic.setNextDirection(direction);
		
		for(int i = 0; i < repeat; i++)
			gameLogic.run(GameLogic.event.none);
	}
	
	private void moveHero(Coord direction)
	{
		GameLogic.event e = GameLogic.getEvent(direction);
		gameLogic.run(e);
	}
	
	@Test
	public void simpleMove() 
	{
		initialize(GameLogic.mode.random);

		// Getting position
		Dragon dragon = dragons.get(0);
		Coord currentPosition = dragon.getPosition().clone();

		// Setting direction
		Coord direction = Coord.north();
		
		// Move dragon
		moveDragon(direction);

		// Calculate final position
		currentPosition.translate(direction.x, direction.y);

		assertTrue(currentPosition.equals(dragon.getPosition()));
		assertTrue(labyrinth.isAvatar(dragon, currentPosition));
	}

	@Test
	public void notMove()
	{
		initialize(GameLogic.mode.random);

		// Getting position
		Dragon dragon = dragons.get(0);
		Coord currentPosition = dragon.getPosition().clone();

		// Move dragon to east
		moveDragon(Coord.east());

		assertTrue(currentPosition.equals(dragon.getPosition()));
		assertTrue(labyrinth.isAvatar(dragon, currentPosition));
	}

	@Test
	public void sleep()
	{
		initialize(GameLogic.mode.randomSleep);

		Dragon dragon = dragons.get(0);
		Coord initialPosition = dragon.getPosition().clone();

		// Dragon sleeps
		testLogic.setDragonSleepBehaviour(true);
		moveDragon(Coord.south());

		// Testing dragon position
		assertTrue(initialPosition.equals(dragon.getPosition()));

		// Testing avatar change
		labyrinth.updateAvatar(dragon);
		assertTrue(labyrinth.getBufferSquare(dragon.getPosition()) == 'd');

		// Testing if hero survives
		moveHero(Coord.south());
	}
	
	@Test
	public void swordOverlap()
	{
		initialize(GameLogic.mode.randomSleep);
		
		moveDragon(Coord.south(), 5);
		
		Coord position = new Coord(1, 8);
		// Testing Dragon + Sword
		assertTrue(labyrinth.getBufferSquare(position) == 'F');
		
		testLogic.setDragonSleepBehaviour(true);
		moveDragon(new Coord(0, 0));
		
		// Testing Dragon sleep + Sword
		assertTrue(labyrinth.getBufferSquare(position) == 'f');
		
		testLogic.setDragonSleepBehaviour(false);
		moveDragon(Coord.north());
		
		// Testing sword avatar
		assertTrue(labyrinth.getBufferSquare(position) == 'E');
		
		// Testing dragon avatar
		Coord north = Coord.north();
		position.translate(north.x, north.y);
		System.out.println(labyrinth);
		assertTrue(labyrinth.getBufferSquare(position) == 'D');
	}
}
