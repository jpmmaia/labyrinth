package gui;

import java.io.Serializable;

import logic.GameLogic;

/**
 * ConfigData.java - This class is used to save the user configurations.
 * 
 * @author Jo�o Maia
 */
public class ConfigData implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	int size;
	boolean defaultMaze;
	
	int numberOfDragons;
	int maxDragons;
	boolean frozenDragon;
	boolean dragonSleepness;
	GameLogic.mode gameMode;
	
	/**
	 * The ConfigData's Constructor
	 * 
	 */
	public ConfigData()
	{
		size = 11;
		defaultMaze = true;
		
		maxDragons = size / 2;
		numberOfDragons = 1;
		gameMode = GameLogic.mode.randomSleep;
	}
	
	/**
	 * 
	 * @return	the size of the labyrinth
	 */
	public int getSize()
	{
		return size;
	}
	
	/**
	 * 
	 * @return	true if the default maze will be built
	 */
	public boolean getDefaultMaze()
	{
		return defaultMaze;
	}
	
	/**
	 * 
	 * @return	the number of dragons the maze will have
	 */
	public int getNumberOfDragons()
	{
		return numberOfDragons;
	}
	
	/**
	 * 
	 * @return	the dragon's mode of the maze which will be built
	 */
	public GameLogic.mode getGameMode()
	{
		return gameMode;
	}
	
	/**
	 * 
	 * @return	the maximun number of dragons the maze can have (based on its size)
	 */
	public int getMaxDragons()
	{
		return maxDragons;
	}
	
	/**
	 * 
	 * @param size	the size of the new maze
	 */
	public void setSize(int size)
	{
		this.size = size;
		this.maxDragons = size / 2;
	}
	
	/**
	 * 
	 * @param defaultMaze	true if the next maze will be the default
	 */
	public void setDefaultMaze(boolean defaultMaze)
	{
		this.defaultMaze = defaultMaze;
	}
	
	/**
	 * 
	 * @param numberOfDragons	the number of dragons the next maze will have
	 */
	public void setNumberOfDragons(int numberOfDragons)
	{
		this.numberOfDragons = numberOfDragons;
	}
	
	/**
	 * 
	 * @param gameMode	the dragon's mode of the next maze
	 */
	public void setGameMode(GameLogic.mode gameMode)
	{
		this.gameMode = gameMode;
	}
	
	/**
	 * Copies all the information of another user configuration data.
	 * 
	 * @param configData	the user configurations to be copied
	 */
	public void copy(ConfigData configData)
	{
		size = configData.getSize();
		numberOfDragons = configData.getNumberOfDragons();
		gameMode = configData.getGameMode();
	}
	
	/**
	 * Makes a new copy of the current configurations.
	 * 
	 * @return	the copy of the current configurations
	 */
	public ConfigData clone()
	{
		ConfigData clone = new ConfigData();
		
		clone.setSize(size);
		clone.setNumberOfDragons(numberOfDragons);
		clone.setGameMode(gameMode);
		
		return clone;
	}
}
