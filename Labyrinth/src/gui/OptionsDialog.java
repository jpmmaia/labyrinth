package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import logic.GameLogic;

/**
 * OptionsDialog.java - This class instantiates a dialog to configure the labyrinth.
 * 
 * @author Jo�o Maia
 */
public class OptionsDialog extends JDialog implements ActionListener, ChangeListener, KeyListener
{	
	private static final long serialVersionUID = 1L;

	JPanel mainPanel;

	JPanel labyrinthMainPanel;
	JCheckBox randomMazeCheckBox;
	JSpinner sizeSpinner;
	SpinnerNumberModel sizeSpinnerModel;

	JPanel dragonMainPanel;
	JRadioButton frozenButton;
	JRadioButton randomButton;
	JRadioButton randomSleepButton;
	JSpinner dragonsSpinner;
	SpinnerNumberModel dragonsSpinnerModel;

	JPanel keysMainPanel;
	JTextField [] keyFields = new JTextField[5];

	JPanel buttonsPanel;
	JButton saveButton;
	JButton cancelButton;

	GameController gameController;
	ConfigData unsavedConfigData;
	ConfigData configData;
	InputData inputData;

	/**
	 * The OptionsDialog's Constructor.
	 * 
	 * @param gameController	the game controller object
	 */
	public OptionsDialog(GameController gameController)
	{
		this.gameController = gameController;
		this.inputData = gameController.getInputData();
		this.configData = gameController.getConfigData();
		this.unsavedConfigData = configData.clone();

		JPanel mainPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		// Labyrinth Panel
		createLabyrinthPanel();
		c.gridx = 0;
		c.gridy = 0;
		mainPanel.add(labyrinthMainPanel, c);

		// Dragon Panel
		createDragonPanel();
		c.gridx = 0;
		c.gridy = 1;
		mainPanel.add(dragonMainPanel, c);

		// Keys Panel
		createKeysPanel();
		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 2;
		mainPanel.add(keysMainPanel, c);

		// Buttons Panel
		createButtonsPanel();
		c.gridx = 0;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 2;
		mainPanel.add(buttonsPanel, c);

		this.setTitle("Options");
		this.setContentPane(mainPanel);
		this.pack();
	}
	
	/**
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) 
	{
		Object source = e.getSource();

		if(source == saveButton)
		{
			configData.copy(unsavedConfigData);
			this.dispose();
		}
		else if(source == cancelButton)
		{
			this.dispose();
		}
		else if(source == frozenButton)
		{
			unsavedConfigData.setGameMode(GameLogic.mode.freeze);
		}
		else if(source == randomButton)
		{
			unsavedConfigData.setGameMode(GameLogic.mode.random);
		}
		else if(source == randomSleepButton)
		{
			unsavedConfigData.setGameMode(GameLogic.mode.randomSleep);
		}
		else if(source == randomMazeCheckBox)
		{
			if(randomMazeCheckBox.isSelected())
			{
				sizeSpinner.setEnabled(true);
				configData.setDefaultMaze(false);
			}
			else
			{
				sizeSpinner.setEnabled(false);
				configData.setDefaultMaze(true);
			}
		}
	}

	/**
	 * 
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	public void stateChanged(ChangeEvent e) 
	{
		Object button = e.getSource();

		if(button == sizeSpinner)
		{	
			unsavedConfigData.setSize(sizeSpinnerModel.getNumber().intValue());

			int maxDragons = configData.getMaxDragons();
			dragonsSpinnerModel.setMaximum(maxDragons);

			if(dragonsSpinnerModel.getNumber().intValue() > maxDragons)
				dragonsSpinner.setValue(maxDragons);
		}
		else if(button == dragonsSpinner)
		{
			unsavedConfigData.setNumberOfDragons(dragonsSpinnerModel.getNumber().intValue());
		}	
	}

	/**
	 * 
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	public void keyTyped(KeyEvent e) 
	{
	}

	/**
	 * 
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	public void keyPressed(KeyEvent e) 
	{
		Object source = e.getSource();

		for(int i = 0; i < keyFields.length; i++)
		{
			if(source == keyFields[i])
			{
				int keyCode = e.getKeyCode();
				inputData.setKey(i, keyCode);

				keyFields[i].setText(KeyEvent.getKeyText(keyCode));
			}
		}
	}

	/**
	 * 
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	public void keyReleased(KeyEvent e) 
	{	
	}

	
	private void createLabyrinthPanel() 
	{
		// Labyrinth Main Panel
		labyrinthMainPanel = new JPanel();
		labyrinthMainPanel.setLayout(new BoxLayout(labyrinthMainPanel, BoxLayout.PAGE_AXIS));
		labyrinthMainPanel.setAlignmentX(CENTER_ALIGNMENT);

		// Label
		JLabel labyrinthLabel = new JLabel("Labyrinth Options");
		labyrinthLabel.setForeground(Color.red);
		labyrinthLabel.setAlignmentX(CENTER_ALIGNMENT);


		// Labyrinth Panel
		JPanel labyrinthPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		// Random Maze Check Box
		randomMazeCheckBox = new JCheckBox("Random Maze");
		randomMazeCheckBox.setSelected(!configData.getDefaultMaze());
		randomMazeCheckBox.addActionListener(this);

		// Size Spinner
		sizeSpinnerModel = new SpinnerNumberModel(configData.getSize(), 11, 25, 2);
		sizeSpinner = new JSpinner(sizeSpinnerModel);
		sizeSpinner.addChangeListener(this);
		sizeSpinner.setEnabled(!configData.getDefaultMaze());

		// Adding components to panel
		c.gridx = 0; c.gridy = 0; c.gridwidth = 2;
		labyrinthPanel.add(randomMazeCheckBox, c);

		c.gridx = 0; c.gridy = 1; c.gridwidth = 1; 
		labyrinthPanel.add(new JLabel("Size"), c);

		c.gridx = 1;
		labyrinthPanel.add(sizeSpinner, c);


		// Adding componenets to main panel
		labyrinthMainPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		labyrinthMainPanel.add(labyrinthLabel);
		labyrinthMainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		labyrinthMainPanel.add(labyrinthPanel);
		labyrinthMainPanel.add(Box.createRigidArea(new Dimension(0, 20)));
	}

	private void createDragonPanel()
	{
		// Dragon Main Panel
		dragonMainPanel = new JPanel();
		dragonMainPanel.setLayout(new BoxLayout(dragonMainPanel, BoxLayout.PAGE_AXIS));
		dragonMainPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

		// Label
		JLabel dragonLabel = new JLabel("Dragon Options");
		dragonLabel.setForeground(Color.red);
		dragonLabel.setAlignmentX(Component.CENTER_ALIGNMENT);


		// Number of Dragons
		JPanel dragonsPanel = new JPanel();
		dragonsPanel.setAlignmentX(CENTER_ALIGNMENT);

		// Dragon's Spinner
		dragonsSpinnerModel = new SpinnerNumberModel(configData.getNumberOfDragons(), 1, configData.getMaxDragons(), 1); 
		dragonsSpinner = new JSpinner(dragonsSpinnerModel);
		dragonsSpinner.addChangeListener(this);

		// Adding components to panel
		dragonsPanel.add(new JLabel("Dragons"));
		dragonsPanel.add(dragonsSpinner);


		// Dragon Mode Panel
		JPanel dragonModePanel = new JPanel();
		dragonModePanel.setAlignmentX(CENTER_ALIGNMENT);

		// Frozen
		frozenButton = new JRadioButton("Frozen");
		frozenButton.addActionListener(this);
		frozenButton.setSelected(configData.getGameMode() == GameLogic.mode.freeze);

		// Sleep Radio Button
		randomButton = new JRadioButton("Random");
		randomButton.addActionListener(this);
		randomButton.setSelected(configData.getGameMode() == GameLogic.mode.random);

		// Do not sleep Radio Button
		randomSleepButton = new JRadioButton("Random & Sleep");
		randomSleepButton.addActionListener(this);
		randomSleepButton.setSelected(configData.getGameMode() == GameLogic.mode.randomSleep);

		// Adding Radio Buttons to a group
		ButtonGroup sleepGroup = new ButtonGroup();
		sleepGroup.add(frozenButton);
		sleepGroup.add(randomButton);
		sleepGroup.add(randomSleepButton);


		// Adding components to panel
		dragonModePanel.add(frozenButton);
		dragonModePanel.add(randomButton);
		dragonModePanel.add(randomSleepButton);


		// Adding components to main panel�
		dragonMainPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		dragonMainPanel.add(dragonLabel);
		dragonMainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		dragonMainPanel.add(dragonsPanel);
		dragonMainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		dragonMainPanel.add(dragonModePanel);
		dragonMainPanel.add(Box.createRigidArea(new Dimension(0, 20)));
	}

	private void createKeysPanel()
	{
		// Keys Main Panel
		keysMainPanel = new JPanel();
		keysMainPanel.setLayout(new BoxLayout(keysMainPanel, BoxLayout.PAGE_AXIS));
		keysMainPanel.setAlignmentX(CENTER_ALIGNMENT);

		// Label
		JLabel keysLabel = new JLabel("Keys");
		keysLabel.setForeground(Color.red);
		keysLabel.setAlignmentX(CENTER_ALIGNMENT);

		// Keys Panel
		JPanel keysPanel = new JPanel();
		keysPanel.setAlignmentX(CENTER_ALIGNMENT);

		// Creating layout
		GroupLayout layout = new GroupLayout(keysPanel);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		keysPanel.setLayout(layout);

		JLabel[] labels = new JLabel[5];

		for(int i = 0; i < keyFields.length; i++)
		{
			labels[i] = new JLabel(InputData.KEY_TEXT[i]);
			keyFields[i] = new JTextField(KeyEvent.getKeyText(inputData.getKey(i)), 8);
			keyFields[i].setHorizontalAlignment(JTextField.CENTER);
			keyFields[i].setEditable(false);
			keyFields[i].addKeyListener(this);

			// Adding labels and text fields
			keysPanel.add(labels[i]);
			keysPanel.add(keyFields[i]);
		}

		// Creating horizontal group
		GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
		GroupLayout.ParallelGroup labelsGroup = layout.createParallelGroup();
		GroupLayout.ParallelGroup fieldsGroup = layout.createParallelGroup();
		for(int i = 0; i < 5; i++)
		{
			labelsGroup.addComponent(labels[i]);
			fieldsGroup.addComponent(keyFields[i]);
		}

		hGroup.addGroup(labelsGroup);
		hGroup.addGroup(fieldsGroup);
		layout.setHorizontalGroup(hGroup);

		// Creating vertical group
		GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
		for(int i = 0; i < 5; i++)
		{
			GroupLayout.ParallelGroup labelFieldGroup = layout.createParallelGroup(Alignment.BASELINE);
			labelFieldGroup.addComponent(labels[i]);
			labelFieldGroup.addComponent(keyFields[i]);
			vGroup.addGroup(labelFieldGroup);
		}
		layout.setVerticalGroup(vGroup);

		keysMainPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		keysMainPanel.add(keysLabel);
		keysMainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		keysMainPanel.add(keysPanel);
		keysMainPanel.add(Box.createRigidArea(new Dimension(0, 20)));
	}

	private void createButtonsPanel()
	{
		// Main Panel
		buttonsPanel = new JPanel();
		buttonsPanel.setAlignmentX(CENTER_ALIGNMENT);

		// Buttons
		saveButton = new JButton("Save");
		saveButton.addActionListener(this);

		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(this);


		// Adding components to panel
		buttonsPanel.add(saveButton);
		buttonsPanel.add(cancelButton); 
		buttonsPanel.add(Box.createRigidArea(new Dimension(0, 20)));
	}
}
