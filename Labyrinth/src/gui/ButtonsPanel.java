package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * ButtonsPanel.java - Panel that contains the main buttons.
 * <p>
 * Buttons: new game, save, load, options, map editor, exit.
 * 
 * @author Jo�o Maia
 */
public class ButtonsPanel extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 1L;
	
	GameController gameController;
	
	JButton newGameButton;
	JButton saveGameButton;
	JButton loadGameButton;
	JButton mapEditorButton;
	JButton optionsButton;
	JButton exitGameButton;
	JFileChooser fileChooser;

	/**
	 * The ButtonsPanel's Constructor 
	 * 
	 * @param gameController	the gameController object
	 */
	public ButtonsPanel(GameController gameController)
	{
		this.gameController = gameController;
		
		newGameButton = new JButton("New Game");
		newGameButton.addActionListener(this);
		newGameButton.setFocusable(false);

		saveGameButton = new JButton("Save Game");
		saveGameButton.addActionListener(this);
		saveGameButton.setFocusable(false);
		
		loadGameButton = new JButton("Load Game");
		loadGameButton.addActionListener(this);
		loadGameButton.setFocusable(false);
		
		mapEditorButton = new JButton("Map Editor");
		mapEditorButton.addActionListener(this);
		mapEditorButton.setFocusable(false);
		
		optionsButton = new JButton("Options");
		optionsButton.addActionListener(this);
		optionsButton.setFocusable(false);

		exitGameButton = new JButton("Exit Game");
		exitGameButton.addActionListener(this);
		exitGameButton.setFocusable(false);
		
		fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Maze File", "maze"));

		this.add(newGameButton);
		this.add(saveGameButton);
		this.add(loadGameButton);
		this.add(mapEditorButton);
		this.add(optionsButton);
		this.add(exitGameButton);
	}

	/**
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) 
	{
		Object button = e.getSource();

		if(button == newGameButton)
			newGameButtonHandler();
		else if(button == optionsButton)
			optionsButtonHandler();
		else if(button == saveGameButton)
			saveGameButtonHandler();
		else if(button == loadGameButton)
			loadGameButtonHandler();
		else if(button == mapEditorButton)
			mapEditorButtonHandler();
		else if(button == exitGameButton)
			exitGameButtonHandler();
	}

	private void newGameButtonHandler()
	{
		int answer = JOptionPane.showConfirmDialog(
				getParent(), 
				"Do you really wish to start a new game?", 
				"New Game", 
				JOptionPane.YES_NO_OPTION);

		switch(answer)
		{
		case JOptionPane.YES_OPTION:
			gameController.newGame();
			break;

		case JOptionPane.NO_OPTION:
			break;
		}

		getParent().setFocusable(true);
	}
	
	private void saveGameButtonHandler() 
	{
		int option = fileChooser.showSaveDialog(this);
		
		if(option == JFileChooser.APPROVE_OPTION)
		{
			File file = fileChooser.getSelectedFile();
			
			String extension = ".maze";
			if(!file.getName().endsWith(extension))
			{
			    file = new File(file + extension);
			}
			
			gameController.saveGameState(file);
		}
	}
	
	private void loadGameButtonHandler() 
	{
		int option = fileChooser.showOpenDialog(this);
		
		if(option == JFileChooser.APPROVE_OPTION)
		{
			File file = fileChooser.getSelectedFile();
			gameController.loadGameState(file);
		}
	}
	
	private void mapEditorButtonHandler()
	{
		gameController.launchMapEditor();
	}

	private void optionsButtonHandler()
	{
		OptionsDialog optionsDialog = new OptionsDialog(gameController);
		optionsDialog.setVisible(true);
	}
	
	private void exitGameButtonHandler()
	{
		int answer = JOptionPane.showConfirmDialog(
				getParent(),
				"Are you sure you want to exit the game?", 
				"Exit Game", 
				JOptionPane.YES_NO_OPTION);
		
		switch(answer)
		{
		case JOptionPane.YES_OPTION:
			System.exit(0);
			break;
		case JOptionPane.NO_OPTION:
			break;
		}
	}
}
