package gui;

import java.awt.event.KeyEvent;
import java.io.Serializable;

/**
 * InputData.java - This class is used to save the user input configurations.
 * 
 * @author Jo�o Maia
 */
public class InputData implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * What each key in the array means
	 */
	public static final String[] KEY_TEXT = 
		{
		"Move East", "Move North", "Move West", "Move South", "Fly"
		};
	
	/**
	 * Contains the number of keys
	 */
	public static final int N_KEYS = 5;

	private int[] keys = new int[N_KEYS];

	/**
	 * The InputData's Constructor.
	 */
	public InputData()
	{
		keys[0] = KeyEvent.VK_D;
		keys[1] = KeyEvent.VK_W;
		keys[2] = KeyEvent.VK_A;
		keys[3] = KeyEvent.VK_S;
		keys[4] = KeyEvent.VK_F;
	}

	/**
	 * The InputData's Constructor which initializes the keys with values
	 * 
	 * @param moveEast		the key to move the hero to the east
	 * @param moveNorth		the key to move the hero to the north
	 * @param moveWest		the key to move the hero to the west
	 * @param moveSouth		the key to move the hero to the south
	 * @param fly			the key to trigger the eagle
	 */
	public InputData(int moveEast, int moveNorth, int moveWest, int moveSouth, int fly)
	{
		keys[0] = moveEast;
		keys[1] = moveNorth;
		keys[2] = moveWest;
		keys[3] = moveSouth;
		keys[4] = fly;
	}
	
	/**
	 * Returns the keyCode correspondent to the index
	 * 
	 * @param index	the index of the key
	 * @return		the correspondent keyCode
	 */
	public int getKey(int index)
	{
		if(index > N_KEYS || index < 0)
			return KeyEvent.VK_NONCONVERT;
		
		return keys[index];
	}
	
	/**
	 * Changes the value of a command
	 * 
	 * @param index		the index of the key
	 * @param keyCode	the keyCode of the key
	 */
	public void setKey(int index, int keyCode)
	{
		if(index > N_KEYS || index < 0)
			return;
		
		keys[index] = keyCode;
	}
}
