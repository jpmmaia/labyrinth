package gui;

import logic.Labyrinth;
import logic.Coord;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

/**
 * MapView.java - This class contains all the images used to represent the map in the GUI.
 * 
 * @author Jo�o Maia
 */
public class MapView extends JComponent implements ComponentListener
{
	static final long serialVersionUID = 1L;
	
	Labyrinth labyrinth;
	transient Image dragon;
	transient Image dragonSleep;
	transient Image dragonSleepSword;
	transient Image dragonSword;
	transient Image sword;
	transient Image hero;
	transient Image heroSword;
	transient Image grass;
	transient Image eagle;
	transient Image eagleSword;
	transient Image eagleDragon;
	transient Image eagleDragonSleep;
	transient Image eagleSwordDragon;
	transient Image eagleSwordDragonSleep;
	transient Image exit;
	transient Image wall;
	
	int cellWidth = 32;
	int cellHeight = 32;
	private int xShift;
	private int yShift;
	private Dimension dimensions;

	/**
	 * The MapView's constructor.
	 * 
	 * @param labyrinth		the labyrinth to be displayed.
	 * @param dimensions	the dimensions of the pane
	 */
	public MapView(Labyrinth labyrinth, Dimension dimensions)
	{	
		this.labyrinth = labyrinth;
		this.dimensions = dimensions;

		center();
		readImages();
	}

	/**
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int width = labyrinth.getWidth();
		int height = labyrinth.getHeight();

		for(int col = 0; col < width; col++)
		{
			for(int line = 0; line < height; line++)
			{
				Coord position = new Coord(col, line);
				char avatar = labyrinth.getBufferSquare(position);

				int x = position.x;
				int y = position.y;

				if(avatar != 'X')
					drawImage(g, x, y, grass);

				switch(avatar)
				{
				case 'D':
					drawImage(g, x, y, dragon);
					break;
				case 'd':
					drawImage(g, x, y, dragonSleep);
					break;
				case 'F':
					drawImage(g, x, y, dragonSword);
					break;
				case 'f':
					drawImage(g, x, y, dragonSleepSword);
				case 'E':
					drawImage(g, x, y, sword);
					break;
				case 'H':
					drawImage(g, x, y, hero);
					break;
				case 'A':
					drawImage(g, x, y, heroSword);
					break;
				case 'B':
					drawImage(g, x, y, eagle);
					break;
				case 'V':
					drawImage(g, x, y, eagleSword);
					break;
				case 'Q':
					drawImage(g, x, y, eagleSwordDragon);
					break;
				case 'q':
					drawImage(g, x, y, eagleSwordDragonSleep);
					break;
				case 'G':
					drawImage(g, x, y, eagleDragon);
					break;
				case 'g':
					drawImage(g, x, y, eagleDragonSleep);
					break;
				case 'X':
					drawImage(g, x, y, wall);
					break;
				case 'S':
					drawImage(g, x, y, exit);
					break;
				}	
			}
		}
	}

	/**
	 * Sets a new labyrinth to be displayed.
	 * 
	 * @param labyrinth	the labyrinth to be set
	 */
	public void setLabyrinth(Labyrinth labyrinth)
	{
		this.labyrinth = labyrinth;
	}

	/**
	 * 
	 * @see java.awt.event.ComponentListener#componentResized(java.awt.event.ComponentEvent)
	 */
	public void componentResized(ComponentEvent e) 
	{
		this.dimensions = e.getComponent().getSize();
		center();
	}

	/**
	 * 
	 * @see java.awt.event.ComponentListener#componentMoved(java.awt.event.ComponentEvent)
	 */
	public void componentMoved(ComponentEvent e) 
	{
	}

	/**
	 * 
	 * @see java.awt.event.ComponentListener#componentShown(java.awt.event.ComponentEvent)
	 */
	public void componentShown(ComponentEvent e)
	{
	}

	/**
	 * 
	 * @see java.awt.event.ComponentListener#componentHidden(java.awt.event.ComponentEvent)
	 */
	public void componentHidden(ComponentEvent e) 
	{
	}
	
	
	private void drawImage(Graphics g, int x, int y, Image image)
	{
		g.drawImage(image, xShift + x * cellWidth, yShift + y * cellHeight, cellWidth, cellHeight, null);
	}

	private void readImages()
	{
		dragon = readImage("images/Dragon.png");
		dragonSleep = readImage("images/DragonSleep.png");
		dragonSleepSword = readImage("images/DragonSleepSword.png");
		sword = readImage("images/Sword.png");
		hero = readImage("images/Hero.png");
		heroSword = readImage("images/HeroSword.png");
		grass = readImage("images/Grass.png");
		eagle = readImage("images/Eagle.png");
		eagleSword = readImage("images/EagleSword.png");
		eagleDragon = readImage("images/EagleDragon.png");
		eagleDragonSleep = readImage("images/EagleDragonSleep.png");
		eagleSwordDragon = readImage("images/EagleSwordDragon.png");
		eagleSwordDragonSleep = readImage("images/EagleSwordDragonSleep.png");
		exit = readImage("images/Exit.png");
		dragonSword = readImage("images/DragonSword.png");
		wall = readImage("images/Wall.png");
	}

	private Image readImage(String filename)
	{
		ImageIcon image = new ImageIcon(filename);

		return image.getImage();
	}

	private void center()
	{
		int width = labyrinth.getWidth();
		int height = labyrinth.getHeight();

		Double wWidth = dimensions.getWidth();
		this.xShift = (wWidth.intValue() - 32 * width) / 2;

		Double wHeight = dimensions.getHeight();
		this.yShift = (wHeight.intValue() - 32 * height) / 2;
	}
}
