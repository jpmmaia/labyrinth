package gui;

import logic.GameLogic;

import java.awt.event.*;
import java.io.Serializable;

/**
 * KBListener.java - This class is the KeyListener of the main frame.
 * <p>
 * It runs the game for each time certain keys are press by the user.
 * 
 * @author Jo�o Maia
 */
public class KBListener implements KeyListener, Serializable
{
	private static final long serialVersionUID = 1L;

	private static GameLogic.event[] EVENTS = 
		{
		GameLogic.event.moveEast, GameLogic.event.moveNorth,
		GameLogic.event.moveWest, GameLogic.event.moveSouth,
		GameLogic.event.fly,
		};
	
	private GameController gameController;
	private GameLogic gameLogic;
	private InputData inputData;

	/**
	 * The KBListener's Constructor
	 * 
	 * @param gameController	the GameController Object
	 */
	public KBListener(GameController gameController)
	{
		this.gameController = gameController;
		this.gameLogic = gameController.getGameLogic();
		this.inputData = gameController.getInputData();
	}


	/**
	 * 
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	public void keyTyped(KeyEvent e)
	{		
	}

	/**
	 * 
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	public void keyPressed(KeyEvent e) 
	{		
		int keyCode = e.getKeyCode();

		for(int i = 0; i < EVENTS.length; i++)
			if(keyCode == inputData.getKey(i))
				gameLogic.run(EVENTS[i]);

		gameController.repaint();
		gameController.checkGameStatus();
	}

	/**
	 * 
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	public void keyReleased(KeyEvent e) 
	{
	}
	
	/**
	 * Used to refresh the references when a new game is loaded.
	 */
	public void reload()
	{
		gameLogic = gameController.getGameLogic();
		inputData = gameController.getInputData();
	}
}
