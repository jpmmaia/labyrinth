package gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/**
 * BoardPanel.java - Panel that contains the maze board.
 * 
 * @author Jo�o Maia
 */
public class BoardPanel extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 1L;
	
	GameController gameController;
	MapView mapView;
	
	/**
	 * BoardPanel's Contructor
	 * 
	 * @param gameController	the game controller object
	 */
	public BoardPanel(GameController gameController)
	{
		this.gameController = gameController;
		gameController.setBoardPanel(this);
		
		// Map View
		Dimension dimensions = new Dimension(500, 500);
		this.setPreferredSize(dimensions);
		mapView = new MapView(gameController.getGameLogic().getLabyrinth(), dimensions);
		this.addComponentListener(mapView);
		
		// Listeners
		this.addKeyListener(gameController.getKBListener());
		
		// Properties
		this.setBackground(Color.BLACK);
		this.setFocusable(true);
	}

	/**
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) 
	{	
		repaint();
	}

	/**
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		mapView.paintComponent(g);
	}
	
	/**
	 * Used to update references when a new game is loaded
	 */
	public void reload()
	{
		mapView.setLabyrinth(gameController.getGameLogic().getLabyrinth());
		repaint();
	}
}
