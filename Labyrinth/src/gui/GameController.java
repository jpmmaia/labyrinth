package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import logic.GameLogic;
import mapEditor.MapEditorButtonsPanel;
import mapEditor.MapEditorPanel;
import random.RandomLogic;
import builder.*;

/**
 * GameController.java - This is the main GUI class controller. 
 * <p>
 * It also keeps the main() function.
 * 
 * @author Jo�o Maia
 */
public class GameController implements Serializable
{
	private static final long serialVersionUID = 1L;

	transient JFrame mainFrame;
	transient BoardPanel boardPanel;
	transient ButtonsPanel buttonsPanel;

	transient MapEditorPanel mapEditorPanel;
	transient MapEditorButtonsPanel mapEditorButtonsPanel;
	
	ConfigData configData = new ConfigData();
	InputData inputData = new InputData();
	transient KBListener kbListener;
	GameLogic gameLogic;
	boolean mapEditorOn = false;

	/**
	 * The GameController's Constructor.
	 */
	public GameController()
	{
		try 
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} 
		catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) 
		{
			e.printStackTrace();
		}

		// GameLogic Setup
		DefaultMazeBuilder builder = new DefaultMazeBuilder();
		gameLogic = builder.build(configData.getGameMode(),
				new RandomLogic(), configData.getNumberOfDragons(), configData.getSize());

		// KBListener Setup
		kbListener = new KBListener(this);

		// Main Frame Setup
		mainFrame = new JFrame("Maze");
		mainFrame.setBackground(Color.black);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Board Panel Setup
		boardPanel = new BoardPanel(this);
		mainFrame.add(boardPanel, BorderLayout.CENTER);

		// Buttons Panel Setup
		buttonsPanel = new ButtonsPanel(this);
		mainFrame.add(buttonsPanel, BorderLayout.SOUTH);

		mapEditorPanel = new MapEditorPanel(this);
		mapEditorButtonsPanel = new MapEditorButtonsPanel(mapEditorPanel);
		
		// Final Main Frame Setup
		mainFrame.pack();
		mainFrame.setVisible(true);
	}

	/**
	 * Launches a new game with the new configurations.
	 */
	public void newGame()
	{
		IMazeBuilder builder = configData.getDefaultMaze() ? new DefaultMazeBuilder() : new MazeBuilder();
		gameLogic.clone(builder.build(configData.getGameMode(),
				new RandomLogic(), configData.getNumberOfDragons(), configData.getSize()));

		repaint();
	}

	/**
	 * Launches the Map Editor.
	 */
	public void launchMapEditor()
	{
		if(!mapEditorOn)
		{
			mainFrame.setVisible(false);
			
			mainFrame.remove(boardPanel);
			mainFrame.remove(buttonsPanel);
			mainFrame.add(mapEditorPanel, BorderLayout.CENTER);
			mainFrame.add(mapEditorButtonsPanel, BorderLayout.SOUTH);
			mapEditorOn = true;
			
			mainFrame.pack();
			mainFrame.setVisible(true);
		}
	}

	/**
	 * Launches the game, when comming from Map Editor.
	 */
	public void launchGame()
	{
		if(mapEditorOn)
		{
			mainFrame.setVisible(false);
			
			mainFrame.remove(mapEditorPanel);
			mainFrame.remove(mapEditorButtonsPanel);
			mainFrame.add(boardPanel, BorderLayout.CENTER);
			mainFrame.add(buttonsPanel, BorderLayout.SOUTH);
			mapEditorOn = false;
			
			mainFrame.pack();
			mainFrame.setVisible(true);
		}
	}

	public void repaint()
	{
		boardPanel.repaint();
	}

	/**
	 * Saves the current state of the game in a file.
	 * 
	 * @param file	the file to save the game
	 */
	public void saveGameState(File file)
	{
		try
		{
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(file));
			os.writeObject(this);
			os.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * Loads a new game from a file which contains the information of the game.
	 * 
	 * @param file	the file to load the game
	 */
	public void loadGameState(File file)
	{
		try 
		{
			ObjectInputStream is = new ObjectInputStream(new FileInputStream(file));
			GameController gameController = (GameController)is.readObject();
			is.close();

			configData = gameController.getConfigData();
			inputData = gameController.getInputData();
			gameLogic = gameController.getGameLogic();

			boardPanel.reload();
			kbListener.reload();
		} 
		catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Checks if the state of the game.
	 * <p>
	 * Checks if the game was won or lost and shows a message dialog.
	 * Then, it starts a new game.
	 */
	public void checkGameStatus()
	{
		GameLogic.state gameStatus = gameLogic.getState();
		
		switch(gameStatus)
		{
		case won:
			JOptionPane.showMessageDialog(
					mainFrame,
					"You won!",
					"Contgratulations!",
					JOptionPane.PLAIN_MESSAGE);
			break;
		case lost:
			JOptionPane.showMessageDialog(
					mainFrame,
					"Better luck next time!",
					"You lost!",
					JOptionPane.PLAIN_MESSAGE);
			break;
		case playing:
			return;
		}
		
		newGame();
	}
	
	/**
	 * Used to clone gameLogic when a new maze is built in the Map Editor
	 * 
	 * @param gameLogic
	 */
	public void reload(GameLogic gameLogic)
	{
		this.gameLogic.clone(gameLogic);
	}
	
	/**
	 * Adjusts the frame to is components.
	 * 
	 * @see JFrame#pack()
	 */
	public void framePack()
	{
		mainFrame.pack();
	}

	/**
	 * 
	 * @return	the gameLogic object
	 */
	public GameLogic getGameLogic()
	{
		return gameLogic;
	}

	/**
	 * 
	 * @return	the KeyListener
	 */
	public KBListener getKBListener()
	{
		return kbListener;
	}

	/**
	 * 
	 * @return	the user input data
	 */
	public InputData getInputData()
	{
		return inputData;
	}

	/**
	 * 
	 * @return	the user configurations data
	 */
	public ConfigData getConfigData()
	{
		return configData;
	}

	/**
	 * 
	 * @return	the main frame
	 */
	public JFrame getMainFrame()
	{
		return mainFrame;
	}

	/**
	 * 
	 * @return	the panel which displays the maze
	 */
	public BoardPanel getBoardPanel()
	{
		return boardPanel;
	}

	/**
	 * 
	 * @return	the bottom panel which has the buttons
	 */
	public ButtonsPanel getButtonsPanel()
	{
		return buttonsPanel;
	}

	/**
	 * 
	 * @param boardPanel	sets a new board panel
	 */
	public void setBoardPanel(BoardPanel boardPanel)
	{
		this.boardPanel = boardPanel;
	}

	/**
	 * Main Funtion
	 * <p>
	 * Creates the GameController Object.
	 * Can be used to start the CLI.
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		new GameController();
	}
}
