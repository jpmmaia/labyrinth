package mapEditor;

import gui.GameController;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import random.RandomLogic;
import logic.Coord;
import logic.Dragon;
import logic.DragonMode1;
import logic.DragonMode2;
import logic.DragonMode3;
import logic.Eagle;
import logic.GameLogic;
import logic.Hero;
import logic.Labyrinth;
import logic.Sword;

/**
 * MapEditorPanel.java - Panel which contains the terrain of the map.
 * 
 * @author Jo�o Maia
 */
public class MapEditorPanel extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 1L;

	GameController gameController;
	Font titlesFont;

	JPanel mapPanel;
	Tile[][] map;

	JPanel tilesPanel;
	TileButton selectedTile;
	TileButton[] tileButtons;

	JPanel settingsPanel;
	JSpinner widthSpinner;
	JSpinner heightSpinner;
	JButton saveSettingsButton;
	JButton cancelSettingsButton;
	JRadioButton frozenButton;
	JRadioButton randomButton;
	JRadioButton randomSleepButton;
	GameLogic.mode tmpGameMode = GameLogic.mode.randomSleep;
	GameLogic.mode gameMode = GameLogic.mode.randomSleep;

	Labyrinth labyrinth;
	Hero hero = null;
	Sword sword = null;
	LinkedList<Dragon> dragons = new LinkedList<Dragon>();
	Coord exit = null;

	public MapEditorPanel(GameController gameController)
	{	
		this.gameController = gameController;
		titlesFont = new Font(Font.SANS_SERIF, Font.BOLD, 14);

		createMapPanel(10, 10);

		this.add(mapPanel);
		createTilesPanel();
		createSettingsPanel();

		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		rightPanel.add(tilesPanel);
		rightPanel.add(settingsPanel);
		this.add(rightPanel);

		labyrinth = new Labyrinth();
	}

	public void tileButtonSelected(TileButton tileButton)
	{
		selectedTile.select(false);

		selectedTile = tileButton;
		selectedTile.select(true);
	}

	public void tileSelected(Tile tile)
	{
		Coord position = tile.getPosition();
		removeAvatar(position);

		switch(selectedTile.getAvatar())
		{
		case 'H':
			if(hero != null)
			{
				Coord oldPosition = hero.getPosition();
				map[oldPosition.y][oldPosition.x].replace(tileButtons[0]);
				hero.setPosition(position);
			}
			else
				hero = new Hero(position, labyrinth);

			break;

		case 'E':
			if(sword != null)
			{
				Coord oldPosition = sword.getPosition();
				map[oldPosition.y][oldPosition.x].replace(tileButtons[0]);
				sword.setPosition(position);
			}	
			else
				sword = new Sword(position, labyrinth);

			break;

		case 'D':
			boolean found = false;
			for(Dragon dragon : dragons)
			{
				if(tile.getPosition().equals(dragon.getPosition()))
				{
					found = true;
					break;
				}
			}

			if(!found)
			{
				Dragon dragon = null;

				switch(gameMode)
				{
				case freeze:
					dragon = new DragonMode1(tile.getPosition(), labyrinth);
					break;
				case random:
					dragon = new DragonMode2(tile.getPosition(), new RandomLogic(), labyrinth);
					break;
				case randomSleep:
					dragon = new DragonMode3(tile.getPosition(), new RandomLogic(), labyrinth);
					break;
				}

				if(dragon != null)
					dragons.add(dragon);
			}
			break;

		case 'S':

			if(exit != null)
				map[exit.y][exit.x].replace(tileButtons[4]);

			exit = tile.getPosition();

			break;
		}
	}

	public TileButton getSelectedTileButton()
	{
		return selectedTile;
	}

	private void createMapPanel(int width, int height)
	{
		mapPanel = new JPanel(new GridBagLayout());
		createMap(width, height);
	}

	private void createMap(int width, int height)
	{
		GridBagConstraints c = new GridBagConstraints();

		JLabel mapLabel = new JLabel("Map Editor");
		mapLabel.setForeground(Color.blue);
		mapLabel.setFont(titlesFont);
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(5, 5, 5, 5);
		c.gridwidth = GridBagConstraints.REMAINDER;
		mapPanel.add(mapLabel, c);

		map = new Tile[height][width];
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 1;
		c.insets = new Insets(0, 0, 0, 0);

		for(int line = 0; line < map.length; line++, c.gridy++)
		{
			for(int col = 0; col < map[line].length; col++, c.gridx++)
			{
				map[line][col] = new Tile('X', "images/Wall.png", new Coord(col, line), this);
				mapPanel.add(map[line][col], c);
			}
			c.gridx = 1;
		}

	}

	private void createTilesPanel()
	{
		tilesPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		JLabel tilesLabel = new JLabel("Objects");
		tilesLabel.setForeground(Color.blue);
		tilesLabel.setFont(titlesFont);
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(5, 5, 5, 5);
		c.gridwidth = GridBagConstraints.REMAINDER;
		tilesPanel.add(tilesLabel, c);

		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(2, 2, 2, 2);

		String path = "images/";
		String[] filenames = 
			{
				"Grass.png", "Hero.png", "Sword.png", "Dragon.png", "Wall.png", "Exit.png"
			};
		char[] avatars =
			{
				' ', 'H', 'E', 'D', 'X', 'S'
			};

		tileButtons = new TileButton[filenames.length];
		for(int y = 0; y < filenames.length; y++)
		{
			tileButtons[y] = new TileButton(avatars[y], path + filenames[y], this);
			tilesPanel.add(tileButtons[y], c);
			c.gridy++;
		}

		selectedTile = tileButtons[0];
		tileButtons[0].select(true);
	}

	private void createSettingsPanel()
	{
		settingsPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		JLabel settingsLabel = new JLabel("Settings");
		settingsLabel.setForeground(Color.blue);
		settingsLabel.setFont(titlesFont);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.insets = new Insets(10, 0, 10, 0);
		settingsPanel.add(settingsLabel, c);

		JLabel widthLabel = new JLabel("Width");
		SpinnerNumberModel widthSpinnerModel = new SpinnerNumberModel(10, 10, 25, 1);
		widthSpinner = new JSpinner(widthSpinnerModel);
		
		JPanel widthPanel = new JPanel();
		widthPanel.add(widthLabel);
		widthPanel.add(widthSpinner);
		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 2;
		c.insets = new Insets(5, 5, 5, 5);
		settingsPanel.add(widthPanel, c);
		
		JLabel heightLabel = new JLabel("Height");

		SpinnerNumberModel heightSpinnerModel = new SpinnerNumberModel(10, 10, 25, 1);
		heightSpinner = new JSpinner(heightSpinnerModel);

		JPanel heightPanel = new JPanel();
		heightPanel.add(heightLabel);
		heightPanel.add(heightSpinner);
		c.gridx = 0;
		c.gridy++;
		settingsPanel.add(heightPanel, c);
		
		// Frozen
		frozenButton = new JRadioButton("Frozen");
		frozenButton.addActionListener(this);
		frozenButton.setSelected(gameMode == GameLogic.mode.freeze);

		// Sleep Radio Button
		randomButton = new JRadioButton("Random");
		randomButton.addActionListener(this);
		randomButton.setSelected(gameMode == GameLogic.mode.random);

		// Do not sleep Radio Button
		randomSleepButton = new JRadioButton("Random & Sleep");
		randomSleepButton.addActionListener(this);
		randomSleepButton.setSelected(gameMode == GameLogic.mode.randomSleep);

		// Adding Radio Buttons to a group
		ButtonGroup sleepGroup = new ButtonGroup();
		sleepGroup.add(frozenButton);
		sleepGroup.add(randomButton);
		sleepGroup.add(randomSleepButton);
		
		JPanel dragonModePanel = new JPanel();
		dragonModePanel.add(frozenButton);
		dragonModePanel.add(randomButton);
		dragonModePanel.add(randomSleepButton);
		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 2;
		settingsPanel.add(dragonModePanel, c);
		
		saveSettingsButton = new JButton("Save");
		saveSettingsButton.addActionListener(this);
		cancelSettingsButton = new JButton("Cancel");
		cancelSettingsButton.addActionListener(this);
		
		JPanel savePanel = new JPanel();
		savePanel.add(saveSettingsButton);
		savePanel.add(cancelSettingsButton);
		c.gridx = 0;
		c.gridy++;
		settingsPanel.add(savePanel, c);
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Object source = e.getSource();

		if(source == saveSettingsButton)
			saveSettingsButtonHandler();
		else if(source == cancelSettingsButton)
			cancelSettingsButtonHandler();
		else if(source == frozenButton)
			tmpGameMode = GameLogic.mode.freeze;
		else if(source == randomButton)
			tmpGameMode = GameLogic.mode.random;
		else if(source == randomSleepButton)
			tmpGameMode = GameLogic.mode.randomSleep;
	}

	private void saveSettingsButtonHandler() 
	{
		int width = (int) widthSpinner.getValue();
		int height = (int) heightSpinner.getValue();
		gameMode = tmpGameMode;

		mapPanel.removeAll();
		createMap(width, height);

		gameController.framePack();
		
		this.validate();
		this.repaint();
		
		hero = null;
		sword = null;
		exit = null;
		dragons = new LinkedList<Dragon>();
	}

	private void cancelSettingsButtonHandler() 
	{
		widthSpinner.setValue(map[0].length);
		heightSpinner.setValue(map.length);
		tmpGameMode = gameMode;
		
		switch(gameMode)
		{
		case freeze:
			frozenButton.setSelected(true);
			break;
		case random:
			randomButton.setSelected(true);
			break;
		case randomSleep:
			randomSleepButton.setSelected(true);
			break;
		}
	}

	public void play()
	{
		if(hero == null || sword == null || dragons.size() == 0 || exit == null)
		{
			JOptionPane.showMessageDialog(
					getParent(), 
					"You need to have one hero, one sword, one exit and at least one dragon.", 
					"Map Editor Error", 
					JOptionPane.ERROR_MESSAGE);
			
			return;
		}
		
		char[][] charMap = new char[map.length][map[0].length];
		for(int line = 0; line < map.length; line++)
		{
			for(int col = 0; col < map[line].length; col++)
			{
				char avatar = map[line][col].getAvatar();
				if(avatar == 'X' || avatar == ' ' || avatar == 'S')
					charMap[line][col] = avatar;
			}
		}

		labyrinth.setMap(charMap);
		labyrinth.setExit(exit);

		Dragon[] dragonsArray = new Dragon[dragons.size()];
		for(int i = 0; i < dragons.size(); i++)
			dragonsArray[i] = dragons.get(i);
		
		GameLogic gameLogic = new GameLogic(labyrinth, hero, new Eagle(hero, sword, labyrinth), sword, dragonsArray);
		gameController.reload(gameLogic);
		gameController.launchGame();
	}

	public void exitEditor()
	{
		gameController.launchGame();
	}
	
	private void removeAvatar(Coord position)
	{
		if(hero != null && hero.getPosition().equals(position))
			hero = null;
		else if(sword != null && sword.getPosition().equals(position))
			sword = null;
		else if(exit != null && exit.equals(position))
			exit = null;
		else
			for(int i = 0; i < dragons.size(); i++)
				if(dragons.get(i).equals(position))
					dragons.remove(i);
	}
}