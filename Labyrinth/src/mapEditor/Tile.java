package mapEditor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;

import logic.Coord;

/**
 * Tile.java - Used to represent a square of the maze in the map editor.
 * 
 * @author Jo�o Maia
 */
public class Tile extends JComponent implements MouseListener
{
	private static final long serialVersionUID = 1L;

	Coord position;
	MapEditorPanel mapEditorPanel;
	char avatar;
	BufferedImage image;
	Border redBorder;

	public Tile(char avatar, String filename, Coord position, MapEditorPanel mapEditorPanel)
	{
		this.avatar = avatar;
		this.position = position;
		this.mapEditorPanel = mapEditorPanel;

		image = readImage(filename);
		redBorder = BorderFactory.createLineBorder(Color.red, 2);

		this.setPreferredSize(new Dimension(32, 32));
		this.addMouseListener(this);
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		g.drawImage(image, 0, 0, 32, 32, null);
	}

	private BufferedImage readImage(String filename)
	{
		BufferedImage image = null;

		try
		{
			image = ImageIO.read(new File(filename));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		return image;
	}

	public Coord getPosition()
	{
		return position.clone();
	}

	public void replace(TileButton tileButton)
	{
		image = tileButton.getImage();
		avatar = tileButton.getAvatar();

		repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) 
	{	
		TileButton tileButton = mapEditorPanel.getSelectedTileButton();

		image = tileButton.getImage();
		avatar = tileButton.getAvatar();

		repaint();

		mapEditorPanel.tileSelected(this);
	}
	
	public char getAvatar()
	{
		return avatar;
	}

	@Override
	public void mousePressed(MouseEvent e) 
	{	
	}

	@Override
	public void mouseReleased(MouseEvent e) 
	{	
	}

	@Override
	public void mouseEntered(MouseEvent e) 
	{	
		setBorder(redBorder);
	}

	@Override
	public void mouseExited(MouseEvent e) 
	{	
		setBorder(null);
	}
}
