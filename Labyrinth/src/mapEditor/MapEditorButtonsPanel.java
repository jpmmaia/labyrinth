package mapEditor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * MapEditorButtonsPanel.java - Panel which contains all the map editor buttons
 * 
 * @author Jo�o Maia
 */
public class MapEditorButtonsPanel extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 1L;
	
	MapEditorPanel mapEditorPanel;
	JButton playButton;
	JButton exitButton;
	
	public MapEditorButtonsPanel(MapEditorPanel mapEditorPanel)
	{
		this.mapEditorPanel = mapEditorPanel;
		
		playButton = new JButton("Play!");
		playButton.addActionListener(this);
		
		exitButton = new JButton("Exit Editor");
		exitButton.addActionListener(this);
		
		this.add(playButton);
		this.add(exitButton);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		Object source = e.getSource();
		
		if(source == playButton)
			playButtonHandler();
		else if(source == exitButton)
			exitButtonHandler();
	}

	private void playButtonHandler() 
	{
		mapEditorPanel.play();
	}
	
	private void exitButtonHandler() 
	{
		mapEditorPanel.exitEditor();
	}
}
