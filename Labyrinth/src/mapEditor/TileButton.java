package mapEditor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.color.ColorSpace;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;

/**
 * TileButton.java - The buttons which are selected in the right panel of the map editor.
 * <p>
 * Each tile button represents a different square of the maze.
 * 
 * @author Jo�o Maia
 */
public class TileButton extends JComponent implements MouseListener
{
	private static final long serialVersionUID = 1L;
	
	private static Border blueBorder = BorderFactory.createLineBorder(Color.blue);
	private static Border redBorder = BorderFactory.createLineBorder(Color.red, 2);

	MapEditorPanel mapEditorPanel;
	int x = 0;
	int y = 0;
	int width = 32;
	int height = 32;
	char avatar;
	boolean selected = false;
	BufferedImage currentImage;
	BufferedImage selectedImage;
	BufferedImage unselectedImage;

	public TileButton(char avatar, String filename, MapEditorPanel mapEditorPanel)
	{
		this.avatar = avatar;
		this.mapEditorPanel = mapEditorPanel;
		
		selectedImage = readImage(filename);

		ColorConvertOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
		unselectedImage = readImage(filename);
		op.filter(unselectedImage, unselectedImage);
		currentImage = unselectedImage;

		this.setBorder(BorderFactory.createLineBorder(Color.blue));
		this.setPreferredSize(new Dimension(32, 32));
		this.addMouseListener(this);
	}

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		g.drawImage(currentImage, x, y, width, height, null);
	}
	
	public BufferedImage getImage()
	{
		return selectedImage;
	}
	
	public char getAvatar()
	{
		return avatar;
	}
	
	private BufferedImage readImage(String filename)
	{
		BufferedImage image = null;

		try
		{
			image = ImageIO.read(new File(filename));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		return image;
	}
	
	public void select(boolean selected)
	{
		this.selected = selected;
		if(selected)
		{
			currentImage = selectedImage;
			this.setBorder(redBorder);
		}
		else
		{
			currentImage = unselectedImage;
			this.setBorder(blueBorder);
		}
		
	}

	public boolean isSelected()
	{
		return selected;
	}

	@Override
	public void mouseClicked(MouseEvent e) 
	{	
		if(!selected)
		{
			select(true);
			mapEditorPanel.tileButtonSelected(this);
			repaint();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) 
	{
	}

	@Override
	public void mouseReleased(MouseEvent e) 
	{
	}

	@Override
	public void mouseEntered(MouseEvent e) 
	{	
		if(!selected)
		{
			currentImage = selectedImage;
			this.setBorder(redBorder);
			repaint();
		}
	}

	@Override
	public void mouseExited(MouseEvent e) 
	{	
		if(!selected)
		{
			currentImage = unselectedImage;
			this.setBorder(blueBorder);
			repaint();
		}
	}
}
