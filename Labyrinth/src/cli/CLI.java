package cli;

import logic.*;
import random.RandomLogic;

import java.util.Scanner;

import builder.*;

/**
 * CLI.java - Command Line Interface to the game.
 * 
 * @author Jo�o Maia
 *
 */
public class CLI
{
	private GameLogic gameLogic;
	private Labyrinth labyrinth;

	private Scanner scanner = new Scanner(System.in);

	/**
	 * The CLI's Constructor.
	 * <p>
	 * It asks for game mode and the size of the labyrinth. 
	 */
	public CLI()
	{
		System.out.println("Welcome to the Labyrinth!");

		GameLogic.mode gameMode;
		System.out.println("\nIn which mode would you like to play?");
		System.out.println("1. Dragon Freeze");
		System.out.println("2. Dragon Random Movement");
		System.out.println("3. Dragon Random Movement + Sleepness");
		switch(readMenuEntry(3))
		{
		case 1:
			gameMode = GameLogic.mode.freeze;
			break;
		case 2:
			gameMode = GameLogic.mode.random;
			break;
		case 3:
			gameMode = GameLogic.mode.randomSleep;
			break;
		default:
			gameMode = GameLogic.mode.randomSleep;
		}

		System.out.println("\nWould you like to choose a maze size?");
		System.out.println("1. Yes");
		System.out.println("2. No");
		if(readMenuEntry(2) == 1)
			initializeLabyrinth(new MazeBuilder(), gameMode, readNumber(), readSize());
		else
			initializeLabyrinth(new DefaultMazeBuilder(), gameMode, readNumber(), 0);

		labyrinth = gameLogic.getLabyrinth();
	}

	/**
	 * Main Loop function
	 * <p>
	 * It prints the labyrinth.
	 * Waits for user input and runs the game. 
	 * Also it checks if the game has been won or lost.
	 * 
	 * @return	true if the game was won, false if the game was lost
	 */
	public boolean run()
	{
		boolean win = false;

		print();

		while(true)
		{
			gameLogic.run(getEvent());

			print();

			if(gameLogic.getState() == GameLogic.state.won)
			{
				// Handle win
				win = true;

				break;
			}
			else if(gameLogic.getState() == GameLogic.state.lost)
			{
				// Handler loose
				win = false;

				break;
			}
		}

		return win;
	}

	private void initializeLabyrinth(IMazeBuilder builder, GameLogic.mode gameMode, int nDragons, int size)
	{	
		gameLogic = builder.build(gameMode, new RandomLogic(), nDragons, size);
	}

	private int readSize()
	{
		int size = 0;
		do
		{
			System.out.println("Please insert an odd number for the size of the labyrinth");

			size = scanner.nextInt();

		} while(size % 2 == 0);

		return size;
	}

	private int readNumber()
	{
		int size = 0;
		System.out.println("Please insert the number of dragons you want");

		size = scanner.nextInt();

		return size;
	}

	private int readMenuEntry(int size)
	{
		int answer = 0;

		do
		{
			answer = scanner.nextInt();
		} while(answer < 1 && answer > size);


		return answer;
	}

	private String getKey()
	{
		String key = scanner.nextLine();

		return key;
	}

	private GameLogic.event getEvent()
	{
		String key = getKey();

		if(key.equals("d") || key.equals("D"))
			return GameLogic.event.moveWest;
		else if(key.equals("w") || key.equals("W"))
			return GameLogic.event.moveNorth;
		else if(key.equals("a") || key.equals("A"))
			return GameLogic.event.moveEast;
		else if(key.equals("s") || key.equals("S"))
			return GameLogic.event.moveSouth;
		else if(key.equals("f") || key.equals("F"))
			return GameLogic.event.fly;

		return GameLogic.event.none;
	}

	private void print()
	{
		System.out.println(labyrinth);
	}
}
