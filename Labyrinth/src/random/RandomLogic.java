package random;

import java.util.Random;

import logic.Coord;

/**
 * RandomLogic.java - Generates random values for dragon behavior logic.
 * 
 * @author Jo�o Maia
 */
public class RandomLogic implements IRandom 
{
	private static final long serialVersionUID = 1L;
	
	private Random random = new Random();
	
	/**
	 * @see IRandom#nextDirection()
	 */
	public Coord nextDirection()
	{
		int nextDir = random.nextInt(4);

		Coord direction = new Coord(0, 0);
		switch(nextDir)
		{
		case 0:
			direction.x = 1;
			break;
		case 1:
			direction.y = 1;
			break;
		case 2:
			direction.x = -1;
			break;
		case 3:
			direction.y = -1;
			break;
		}
		
		return direction;
	}
	
	/**
	 * @see IRandom#dragonSleepProb()
	 */
	public boolean dragonSleepProb()
	{	
		// 33% chance
		int sleepProb = random.nextInt(100) + 1;
		if(sleepProb < 33)
			return true;
		
		return false;
	}
}
