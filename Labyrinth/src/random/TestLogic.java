package random;

import logic.Coord;

/**
 * TestLogic.java - Used for unit testing purposes
 * 
 * @author Jo�o Maia
 */
public class TestLogic implements IRandom
{
	private static final long serialVersionUID = 1L;
	private Coord direction = new Coord(0, 0);
	private boolean sleep = false;
	
	/**
	 * @see IRandom#nextDirection()
	 */
	public Coord nextDirection()
	{
		return direction;
	}
	
	/**
	 * @see IRandom#dragonSleepProb()
	 */
	public boolean dragonSleepProb()
	{
		return sleep;
	}
	
	/**
	 * Used to test the dragon's movement behavior.
	 * 
	 * @param direction	the next direction the dragon will follow
	 */
	public void setNextDirection(Coord direction)
	{
		this.direction = direction;
	}
	
	/**
	 * Used to test the dragon's sleepness behavior.
	 * 
	 * @param sleep	true if the dragon sleeps or stays awaken next round
	 */
	public void setDragonSleepBehaviour(boolean sleep)
	{
		this.sleep = sleep;
	}
}
