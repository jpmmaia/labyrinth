package random;

import java.io.Serializable;

import logic.Coord;

/**
 * IRandom.java - Interface for the dragon random logic and testing purposes
 * 
 * @author Jo�o Maia
 */
public interface IRandom extends Serializable
{
	/**
	 * Generates a random direction (East, North, West, South).
	 * 
	 * @return the next direction which the dragon will move
	 * @see Coord#east()
	 * @see Coord#north()
	 * @see Coord#west()
	 * @see coord#south()
	 */
	public Coord nextDirection();
	
	/**
	 * Returns a random boolean which will tell if the dragon falls asleep.
	 * 
	 * @return	true if the dragon will sleep
	 */
	public boolean dragonSleepProb();
}
